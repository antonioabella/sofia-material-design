<a name="0.1.0"></a>
# 0.1.0 (2015-11-19)


### bug

* bug(searchbox): not searching after angular update to 1.4.1 4889348

### chore

* chore: A bunch of new files accf4b4
* chore: release 0.1.0 952606d
* chore(bower): angular-material 0.11.1 f200a82
* chore(bower): delete mdi dependency 39af131
* chore(bower): delete pdfmake 3cec74e
* chore(bower): material af185d3
* chore(build): clean task deletes dist.zip 23bb1f5
* chore(gulp): Watch changes in new jade/coffee b4b8df3
* chore(images): sample images 1292867
* chore(index.html): delete comment for bower:css inject 13275ae
* chore(karma.conf.coffee): upgrade to coverage 0.5.0 3f66ab1
* chore(pachage): ibrik like direct dependency a6e7045
* chore(package): upgrade f75bb9c
* chore(router): Using ui-router instead ngNewRouter f2333e5

### feac

* feac(ActorService): set birthDate to Date on load and save f6f0f59

### feat

* feat: create new custody aac9365
* feat(_container-mixin): dry md-container input 6fe098b
* feat(Actor): error constants d51a30b
* feat(Actor): i18n a79eadb
* feat(Actor): show .page-content style c0792a1
* feat(ActorController): actor.id 68d5a8d
* feat(ActorLabel): link to actor f4e7fb9
* feat(ActorLabel): show identity card 486767b
* feat(ActorService): default parameter and getIdentityCard method 9c9ba3c
* feat(ActorService): getGender, getAge, getBirthDate 64cd298
* feat(ActorService): save and update 0c6c86e
* feat(ActorService): what kind of actor is 21de718
* feat(angular-busy): add busy indicator b4e9540
* feat(AppModule): sofia.enrollment ba62f12
* feat(Billing): services 3a8ca77
* feat(Billing): show debt edf206f
* feat(ConceptToPayService): show ConceptToPay instead Service when debug 4009acd
* feat(dashboard): go to actor page when selected in searchbox 549125d
* feat(dashboard): Icons fd1ca44
* feat(dashboard): Responsive dashboard 16eb416
* feat(DashboardDirectiveController): create actor 57d638d
* feat(DataService): add debug information when list 9c8adfb
* feat(DataService): copy method 3129f2b
* feat(DataService): save and update 001c04b
* feat(DebtBalance): toString 4db865e
* feat(DebtCalculator): calculate debt concepts cef9c8f
* feat(DialogService): deleteItem method d7760f8
* feat(ErrorHandler): use $mdToast to show messages d2d0e94
* feat(genericSelect): replace a lot of directives and services a186048
* feat(genericSelect): replace a lot of directives and services 4d462c7
* feat(genericSelect): replace a lot of directives and services c290d24
* feat(GenericSelectDirective): allow disabled attribute ebcb2c9
* feat(goHome): Button to go to home page f8baef1
* feat(GoHome): color and style d29ac12
* feat(GoHome): font size 247c0b6
* feat(goHome): title 5f1f216
* feat(i18n): add labels for generic controls 7d66523
* feat(images): magnyfy, printer-shadow 76b7a75
* feat(index.less): auxiliary class 779ee88
* feat(index.less): text align 6aa8baa
* feat(InvoiceDashboard): use md-datepicker instead input 4e9fd40
* feat(Months): directive to list and select months 5ef81b8
* feat(Months): month-changed event 85adf09
* feat(Months): permit user to define label 150e971
* feat(MonthService): cache results b3909ec
* feat(MonthService): toString ac5880d
* feat(personFilter): transform person properties to view b10d42e
* feat(PersonService): getFullName 3e82fdb
* feat(SchoolYear): add label bc3df5e
* feat(SchoolYears): yearChanged event bba3f3d
* feat(SchoolYearsDirective): returns year 41da083
* feat(SchoolYearService): toString 52500be
* feat(searchbox): Better behavior and appearance c60a931
* feat(searchbox): emit the sf-selected-item-change($item) d4b358e
* feat(searchbox): Find Actor by name, id-card, etc cdc0f1a
* feat(searchbox): Sugestion control is wide 30fe6d5
* feat(SearchboxDirective): a more generic searchbox 93fa408
* feat(SearchboxDirective): show selected actor fullname f8ce278
* feat(SectionService): clean enrollments before save c87866f
* feat(SOFIA_API): enrollments edff2f0
* feat(sofia-api): guardians/students and students/guardians fe55a00
* feat(style): dl, dt, dd 17de294
* feat(styles): page-content-toolbar b29b47b
* feat(svg): add new images fbb81c0
* feat(svg): add new images d4e66ea

### fix

* fix: prefix directives attributes with sf 882214c
* fix: prefix directives attributes with sf a61d351
* fix(_md-dialog.jade): labels with single quote fails 4a6a12e
* fix(_md-dialog): allow big dialogs 2055ad8
* fix(Actor): actor is not updated 39038d2
* fix(Actor): edit md-datepicker flex 25e3ed9
* fix(actorEdit): delete form in template c7a0b37
* fix(ActorEdit): error showing md-datepicker with label 6af9eb9
* fix(ActorLabel): use button instead link to return to actor show 3879b4b
* fix(ActorService): moment library deprecated issue 7922e0b
* fix(ActorShow): md-scroll-mask workaround 3b0b4d0
* fix(ActorShow): refresh show page after changes 0e2d64d
* fix(ActorSummaryTab): when deployed doesn't show full info 0b5e0b3
* fix(ActorToolbar): use mdDialog instead of page 9f59dd4
* fix(ActorToolbarDirective): remove duplicated Controller 3b70ce8
* fix(ActorWork): edit dialog e249e01
* fix(angular-locale): shortDate e456885
* fix(BanksDirective): return bank when selection change 0200065
* fix(Billing): filterByMonth b23c086
* fix(BillingConfig): disable pay button while processing pay 259c991
* fix(BillingController): use defineProperties instead change event 655b0eb
* fix(BillingMonthService): always add first month to list 313e922
* fix(BillingMonthService): duplicated enrollment month 37f4065
* fix(BillingMonthService): error when month is null a2a9430
* fix(BillingMonthService): TypeError in get method e3acea3
* fix(BillingPayment): _.sum is not being recognized by tests 1226a62
* fix(BillingPayment): inverted pay button logic 85c45dc
* fix(BillingPaymentDirective): send payments to server df1c080
* fix(Build): error building and running tests 04a4b33
* fix(CountriesDirective): drop watchers 500 -> 10 6743f47
* fix(Custody): dialog too small 3baeeba
* fix(CustodyDirectiveController): $inject without $ 37a4341
* fix(dashboard): icon 7fd47fa
* fix(DashboardDirectiveController): firefox bug when create actor 48ec33e
* fix(DataService): clean items before query 90a6b16
* fix(DataService): clean log messages 9a5d02f
* fix(DataService): delete $resource reference in test ec070d6
* fix(Debt): use angular material 97f98a0
* fix(FixturesLoader): config file to configurate fixturePath 8e0e8cd
* fix(GoHome): add md-icon-button class to button 19ed6a1
* fix(goHome): aria label 2d6998c
* fix(GoHome): aria-label 1987d41
* fix(GoHome): home icon instead arrow-left 3b8e6d5
* fix(GoHome): show arrow icon 0bf3c02
* fix(GradesDirective): best behavior 3864fee
* fix(GuardiansDirective): drop watchers 6c09093
* fix(HouseHoldService): mock error 92a4e49
* fix(HouseholdService): not filtering paid concepts 323e93e
* fix(images): use solid version instead shadowed 8d66f2b
* fix(months): setting last month: august 75036a3
* fix(multiple): minor changes and clean e1579ae
* fix(PaymentMethodsDirective): return paymentMethod when selection change f99fdf5
* fix(PersonCardDirective): two way binding ee4a4e5
* fix(PrintInvoice): remove download message error d33267c
* fix(Report): solvence month position 3c00e46
* fix(Report): solvence month position b02cf0c
* fix(sandbox): remove from repository 10eb138
* fix(SandboxController): clean log messages 65e9fca
* fix(SchoolYearsDirective): change yearId to id 2e57fdf
* fix(SearchBox): angular material upgrade version 03bc97d
* fix(SearchBox): clean unused code 32f0dd6
* fix(searchbox): more responsive 7a721ae

### refactor

* refactor: CustodyService replace StudentGuardians and ActorStudentsDirective 3243c5d
* refactor: move methods from actorService to personService 7572e62
* refactor: unified behavior of md-select directives f861ba6
* refactor(ActorCard): rename to PersonCard 293dd17
* refactor(ActorCardDirectiveController): inline controller 7906dfe
* refactor(ActorController): now ActorShowController 3387710
* refactor(ActorEditController): eliminate duplication 094a827
* refactor(ActorEditController): only one controller for every action f4a6e6c
* refactor(actorEditController): save actor instead person fd46d1b
* refactor(ActorService): move tasks to new class PersonService 859f0be
* refactor(ActorShowController): delete 'enumerable: true' from properties 567859a
* refactor(ActorShowController): delete id property f197f65
* refactor(ActorShowController): replace Object.defineProperties d4b8c0e
* refactor(ActorShowController): split in components 5dc3938
* refactor(ActorToolbarDirectiveController): del middle Controller class 7eac0a4
* refactor(app.config): delete unused configurations c1db704
* refactor(BillingButton): call api in set actor property de0e7b5
* refactor(BillingButtonDirective): actor instead actorId 4d8cbd8
* refactor(BillingConfig): change path 084c52d
* refactor(BillingConfig): change variable name config instead of v 0299792
* refactor(BillingController): use solvent instead hasReceivables e203ff9
* refactor(core.config): remove unused variables d117aef
* refactor(CostService): private members 0086ad9
* refactor(DiscountService): private members 7441bbe
* refactor(GoHomeDirective): delete GoHomeDirectiveController ffd0612
* refactor(i18n): create general file a01c880
* refactor(IconListItemDirective): eliminate redundancy 244429a
* refactor(images): remove pictures cb81000
* refactor(InvoiceDashboardController): delete middleman _loadInvoices 2197130
* refactor(mdDialog): use jade extends capabilities 5f11201
* refactor(mdDialog): use jade extends capabilities fe2bfaf
* refactor(MonthsDirectiveController): syntax sugar 64b0519
* refactor(PersonCard): PersonCard instead ActorCard 10d8647
* refactor(PersonService): move getGenderIconName to PersonService 1f4a469
* refactor(PrintReportDirective): use locals to pass report to builder 56ea0ee
* refactor(SchoolYearDirectiveController): syntax sugar a8a00d1

### refactoring

* refactoring: searchboxController -> searchboxDirectiveController dfd1143
* refactoring(ActorLabel): remove unused dependencies ba38c5e
* refactoring(Billing): Modular ed97f38
* refactoring(BillingMonthService): delete dependency in MonthService a7e456f
* refactoring(Modularization): Actor Component 1613431
* refactoring(SchoolYears): clean unused dependencies 70222ae
* refactoring(searchbor): simpler Actor.list call 2f86781

### style

* style(ActorLabel): too much space between lines 627a09f
* style(searchbox): better format to searchbox.tpl.jade 9c93b48

### test

* test(ActorCreateController): should show create dialog 90e2bc0
* test(ActorEditController): coverage db29348
* test(ActorEditController): coverage 30fc36e
* test(ActorEditController): delete PersonService dependency ad00d03
* test(ActorGuardianDirectiveController): coverage d8d434a
* test(ActorLabelDirectiveController): coverage 18f085e
* test(ActorService): coverage bb4ea72
* test(BanksDirectiveController): coverage d491796
* test(BillingController): coverage 605b119
* test(BillingPAymentDirective): coverage bd62334
* test(BillingTotalDirectiveController): coverage 796851a
* test(BillpayDirectiveController): coverage a3b0a7a
* test(ConceptToPay): clean mocks 0c54124
* test(ConceptToPayMocks): clean e8cd7bb
* test(ConceptToPayWithDiscountMocks): clean mocks b1d1266
* test(cost.json): add cost c55af42
* test(Cost): clean mocks 678102e
* test(DataService): save and update aa69059
* test(enrollmentsCommitments): set august like final month 42b217b
* test(MonthDirectiveController): coverage e6be90a
* test(MonthService): getDefault called without months 43a0e59
* test(PaymentMethodsDirectiveController): coverage 3c53751
* test(PersonService): coverage 853c48a
* test(PersonService): delete spec for birthDate bf3c863
* test(SchoolYearsDirectiveController): coverage a97e3d0

* chore libraries update 85ea762
* Chore(bower, package): upgrade 604af8e
* chore(bower,package) 15a4406
* chore(bower) ca9110e
* chore(bower) 117ca55
* chore(bower) df2bcc0
* chore(bower) Reference to angular-ui-router 7ca7321
* chore(bower) update angular 1.4.0-rc.2 ce9ba51
* chore(build): 9ca2ffd
* chore(cloc) 42bced2
* chore(karma.conf.coffee) 5c3dace
* chore(package.json) a4a5391
* chore(package) 6eadc93
* chore(package) 68c21a8
* chore(package) 7104cab
* chore(package) 2f9dfd4
* chore(package) 0c0318e
* chore(package) a68a940
* chore(package) e8469c6
* chore(package) ca5040e
* chore(package) bdc7286
* chore(unit-test.coffee) 3db7e75
* clean(SectionListController): d5d004d
* feat ActorComponent 7ce0a83
* feat Billing Component 440a5e9
* feat MonthService e868a8d
* feat SchoolYearService 9bd4920
* feat($mdDateLocaleProvider): 8c99f79
* feat(ActorCard) ed071d4
* feat(ActorCardDirective) 734a19b
* feat(ActorCreateController) 9a784fd
* feat(ActorEnrollmentsDirective) 25f31c3
* feat(ActorGuardiansDirective) 303aa30
* feat(ActorGuardiansDirective) d7e450f
* feat(ActorGuardianService) 754336e
* feat(ActorLabel) 9fad1ab
* feat(ActorStudentsDirective) e5e563f
* feat(ActorSummaryTabDirective) 7a2f52e
* feat(ActorTax) 9914981
* feat(ActorToolbar) 95b12e2
* feat(ActorWork) 88906b5
* feat(angular-loading-bar) 1c4ba03
* feat(Banks) b6e7555
* feat(BillingButton) c16e21b
* feat(BillingButton) b1f051a
* feat(BillingController) db9aa5e
* feat(BillingMonthService) 888527e
* feat(BillingTotalDirective) 2af77a8
* feat(BillpayDirective) ab9a43b
* feat(BillPayment) 6fff5c2
* feat(ChecklistDirectiveController) 0cbd2d9
* feat(ConceptsDirective) 917c4a9
* feat(ConceptToPayCalculator) ce1e876
* feat(CostService) d318c09
* feat(CountriesDirective) 6844e15
* feat(CountryService) 12121d7
* feat(DebtBalanceService) c0905ab
* feat(DebtCalculator) 79067c1
* feat(DebtDirective) b721702
* feat(DebtDirectiveController) 6868c05
* feat(DialogService) 3c56660
* feat(DiscountService) 3efb7be
* feat(Enrollment) f4ed219
* feat(EnrollmentModule) 949781d
* feat(ErrorHandler) 7bbe441
* feat(favicon) ff2631b
* feat(GendersDirective) 7d54344
* feat(GradesDirective) c5a07f6
* feat(GuardianStudentsService) 25ab35d
* feat(HouseholdService) f41aa9b
* feat(IconListItemDirective) 3c707a2
* feat(images) 7979d05
* feat(InvoiceComponent) 745ef98
* feat(InvoiceDashboard) 8979605
* feat(KinshipsDirective) 54bdac6
* feat(menu-mixin.jade) 7b2a0b5
* feat(menu-mixin.jade) e8549c2
* feat(MonthService, SchoolYearService): caching results 02d0387
* feat(PaymentMethod) 346f56d
* feat(PrintInvoiceDirective) 92256fb
* feat(PrintReportDirective) 85edec6
* feat(ReceivableService) 62071b7
* feat(ReportComponent) fb82bd8
* feat(SchoolYears) 0631e74
* feat(Section): 8eaa8e4
* feat(SectionListController) 1ee1766
* feat(SectionListController): 92aa43e
* feat(SectionsDirective) 43992e2
* feat(StudentEnrollmentsService) 0ae2b88
* feat(StudentGuardians) cb3cedb
* fix(actor/show.jade): cb8b310
* fix(Billing) 35cebe8
* fix(BillingRequest) 50e1403
* fix(BillPay) f0325ab
* fix(DebtDirective) 69e359e
* fix(DialogService): 90ec64a
* fix(InvoiceDashboard): d48e824
* fix(layout): 1b26344
* fix(MonthDirective) 92c5fae
* fix(PrintInvoiceDirectiveController): b80b32c
* fix(ReceivableService) 0a3fe26
* fix(SearccboxDirective) d2bb8af
* fix(SectionListController): b39d6f5
* Initial Commit 9c7c0a4
* Merge branch 'cleaning' into develop 4883f2d
* Merge branch 'cleaning' into develop 0eac28d
* refactor(ActorCardDirectiveController): 7b6de8e
* refactor(DataServiceSpec) 789ceff
* refactor(Views) cf0a4ab
* refactoring new src/directives and app/services sources 730b4e0
* style(BillingButton) ffe940c
* style(GoHome) 95ddabe
* style(GoHomeDirective) 3920843
* style(MonthDirectiveController) 04d340b
* test(ActorController) 9aba595
* test(BankDirectiveController) df026c2
* test(BillingRequest) 16bf998
* test(ConceptCostService) 4e7d085
* test(ConceptService) 456a2a3
* test(CostService) f5519ca
* test(DataService) 3f19f2f
* test(MonthService) 71055cf
* test(PaymentMethodDirectiveController) 579961b
* test(PaymentService) dc3705e
* test(ReceivableService) 6a073dd
* test(toString) 68b6c5c



