'use strict'

angular.module 'sofia.invoice'

.config ['$stateProvider', ($stateProvider) ->
    $stateProvider.state 'invoice',
      url: '/invoice'
      templateUrl: 'components/invoice/dashboard.html'
      controller: 'InvoiceDashboardController'
      controllerAs: 'invoiceDashboard'
]
