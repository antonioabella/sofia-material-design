'use strict'

describe 'InvoiceDashboardController', ->
  controller = {}
  Invoice = {}
  invoices = [status: name: 'ALL']

  beforeEach ->
    module 'sofia'

    inject ($controller, _Invoice_) ->
      controller = $controller 'InvoiceDashboardController'
      Invoice = _Invoice_
      spyOn Invoice, 'list'
        .and.returnValue(createPromise(invoices))

  it 'load invoices on date change', ->
    date = '2050/01/01'
    controller.date = date
    controller.dateChange()
    expect(Invoice.list).toHaveBeenCalledWith(date: date)
    expect(controller.totals.ALL).toBe(1)

  it 'should load invoice by number', ->
    controller.number = 'irrelevant'
    controller.searchInvoiceByNumber()
    expect(Invoice.list).toHaveBeenCalledWith(q: controller.number)

  it 'should know if has invoices', ->
    expect(controller.hasInvoices()).toBeFalsy()
    Invoice.items = ['irrelevant']
    expect(controller.hasInvoices()).toBeTruthy()

  it 'should return invoices', ->
    Invoice.items = invoices
    expect(controller.invoices).toEqual(invoices)

  it 'should return searchPromise', ->
    expect(controller.searchPromise).not.toBeNull()

  it 'should to know when invoice is reversed', ->
    expect(controller.isReversed(status: name: 'irrelevant')).toBeFalsy()
    expect(controller.isReversed(status: name: 'REVERSED')).toBeTruthy()

  it 'should reverse invoice', inject ($mdDialog) ->
    spyOn $mdDialog, 'confirm'
      .and.returnValue createConfirmDialogMock()
    spyOn $mdDialog, 'show'
      .and.returnValue createPromise()
    item = jasmine.createSpyObj('item', ['$delete'])
    item.$delete
      .and.returnValue createPromise()
    Invoice.items = [item]
    controller.reverseInvoice(0)
    expect($mdDialog.confirm).toHaveBeenCalled()
    expect($mdDialog.show).toHaveBeenCalled()
    expect(item.status.name).toBe 'REVERSED'
    expect(item.status.enumType).toBe 'sofia.enums.InvoiceStatus'

  createConfirmDialogMock = ->
    parent: -> return this
    title: -> return this
    content: -> return this
    ariaLabel: -> return this
    ok: -> return this
    cancel: -> return this
