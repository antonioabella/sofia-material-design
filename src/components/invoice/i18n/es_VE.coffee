'use strict'

translations =
  PAID: 'Pagada'
  OPEN: 'Por Pagar'
  REVERSED: 'Anulada'
  INVOICE:
    INVOICES_LABEL: 'Facturas'

angular.module 'sofia.billing'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]
