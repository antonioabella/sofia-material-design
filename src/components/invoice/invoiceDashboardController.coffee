'use strict'

InvoiceDashboardController = ($mdDialog, Invoice) ->
  _totals = {}
  _searchPromise = {}
  _loadInvoiceByDate = (date) ->
    _searchPromise = Invoice.list(date: date).then _calculateTotals
  _loadInvoiceByNumber = (number) ->
    _searchPromise = Invoice.list(q: number).then _calculateTotals
  _calculateTotals = (invoices ) ->
    _totals = _.countBy(invoices, 'status.name')
    _totals.ALL = _.size invoices

  _createConfirm = ->
    $mdDialog.confirm()
      .parent(angular.element(document.body))
      .title '¿Desea anular la factura?'
      .content 'La factura anulada no podrá volver a su estado original'
      .ariaLabel 'Anular Factura'
      .ok 'Si, anular la factura'
      .cancel 'Cancelar anulación'

  class Controller
    constructor: ->
      @date = new Date()
      _loadInvoiceByDate @date

    Object.defineProperties @::,
      invoices: get: -> Invoice.items
      totals: get: -> _totals
      searchPromise: get: -> _searchPromise

    hasInvoices: -> (_.size Invoice.items) > 0
    dateChange: ->
      _loadInvoiceByDate @date
      @number = ''
    searchInvoiceByNumber: ->
      _loadInvoiceByNumber @number
      @date = null
    isReversed: (i) -> i.status.name is 'REVERSED'
    reverseInvoice: (index) ->
      confirm = _createConfirm()
      $mdDialog.show(confirm).then ->
        i = Invoice.items[index]
        i.$delete().then ->
          i.status = enumType: 'sofia.enums.InvoiceStatus', name: 'REVERSED'
          _calculateTotals(Invoice.items)

  new Controller()

InvoiceDashboardController.$inject = ['$mdDialog', 'Invoice']

angular.module 'sofia.invoice'
  .controller 'InvoiceDashboardController', InvoiceDashboardController
