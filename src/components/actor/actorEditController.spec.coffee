'use strict'

describe 'ActorEditController', ->
  controller = {}
  Dialog = {}
  Actor = {}
  person = id: 'irrelevant'
  actor = id: 'irrelevant', person: person
  dialog = jasmine.createSpyObj 'Dialog', ['save']

  beforeEach ->
    module 'sofia'

    inject ($controller, _Dialog_, _Actor_) ->
      Actor = _Actor_
      Actor.item = actor
      Dialog = _Dialog_
      spyOn Dialog, 'create'
        .and.returnValue dialog

      controller = $controller 'ActorEditController'
      expect(Dialog.create).toHaveBeenCalled()

  it 'should save actor and close dialog when success', ->
    controller.save()
    expect(dialog.save).toHaveBeenCalled()
