'use strict'

ActorShowController = ($stateParams, Actor) ->
  masks = document.getElementsByClassName('md-scroll-mask')
  masks[0].remove() if not _.isEmpty(masks)

  Actor.get id: $stateParams.id

  class Controller
    Object.defineProperties @::,
      actor: get: -> Actor.item

  new Controller()

ActorShowController.$inject = ['$stateParams', 'Actor']

angular.module 'sofia.actor'
  .controller 'ActorShowController', ActorShowController
