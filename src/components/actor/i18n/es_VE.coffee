'use strict'

translations =
  FATHER: 'Padre'
  MOTHER: 'Madre'
  BROTHER: 'Hermano'
  SISTER: 'Hermana'
  UNCLE: 'Tío'
  AUNT: 'Tía'
  GRANDFATHER: 'Abuelo'
  GRANDMOTHER: 'Abuela'
  OTHER: 'Otro'

  sofia:
    Guardian: 'Representante'
    Student: 'Estudiante'
    Teacher: 'Profesor'
  PERSON:
    PERSONAL_DATA_LABEL: 'Datos Personales'
    CONTACT_DATA_LABEL: 'Datos de Contacto'
    WORK_DATA_LABEL: 'Datos Laborales'
    BIRTH_COUNTRY_LABEL: 'País de Nacimiento'
    CHANGE_PICTURE_LABEL: 'Cambiar Foto'
    FULL_NAME_LABEL: 'Apellidos y Nombres'
    FIRST_NAME_LABEL: 'Nombres'
    LAST_NAME_LABEL: 'Apellidos'
    GENDER_LABEL: 'Sexo'
    BIRTH_DATE_LABEL: 'Fecha de Nacimiento'
    ADDRESS_LABEL: 'Dirección'
    OUTSIDER_LABEL: 'Extranjero'
    NOT_OUTSIDER_LABEL: 'Venezolano'
    NOTES_LABEL: 'Observaciones'
    IDENTITY_CARD_LABEL: 'Cédula'
    PHONE_NUMBERS_LABEL: 'Teléfonos'
    PHONE_NUMBER_LABEL: 'Teléfono'
    EMAILS_LABEL: 'Emails'
    EMAIL_LABEL: 'Email'
    FACEBOOK_LABEL: 'Facebook'
    TWITTER_LABEL: 'Twitter'
    NOT_FOUND_MSG: 'Ninguna persona cumple con el criterio de búsqueda'
    SEARCH_LABEL: 'Buscar por cédula, nombres, apellidos y/o email'
    SEARCH_BY_IDENTITY_CARD_LABEL: 'Buscar por cédula'
    OCCUPATION_LABEL: 'Profesión'
    WORKPLACE_LABEL: 'Lugar de Trabajo'
    POSITION_LABEL: 'Cargo'
    SEARCH_MESSAGE: '''
      El sistema verificará la existencia de la persona. Coloque la cédula
      y pulse `Buscar`. Si la persona existe, pulse 'Agregar', si no pulse el
      botón `Nuevo`
    '''
  ACTOR:
    SUMMARY_LABEL: 'Resumen'
    REFERENCES_LABEL: 'Referencias'

  GUARDIAN:
    BILLING_DATA_LABEL: 'Datos Facturación'
    STUDENTS_LABEL: 'Estudiantes'
    STUDENTS:
      ADD_LABEL: 'Agregar Estudiante'
    KINSHIP_DATA_LABEL: 'Datos Filiatorios'
    KINSHIP_LABEL: 'Parentesco'
    TAX_PAYER_NAME_LABEL: 'Factura a nombre de:'
    TAX_IDENTITY_LABEL: 'RIF/C.I.'
    TAX_DOMICILE_LABEL: 'Dirección Fiscal'

  STUDENT:
    GUARDIANS_LABEL: 'Representantes'
    GUARDIANS:
      ADD_LABEL: 'Agregar Representante'
      EMPTY_MESSAGE: '''El estudiante no tiene representantes asignados.
        Por favor, asigne un representante al estudiante'''
    ENROLLMENT:
      EDIT_LABEL: 'Editar datos de Inscripción'
      WITHDRAWAL_LABEL: 'Retirar Estudiante'
    ENROLLMENTS_LABEL: 'Inscripciones'
    CURRENT_ENROLLMENT_LABEL: 'Inscripción Actual'
    CURRENT_ENROLLMENT_EMPTY_MESSAGE: '''El estudiante no está inscrito
      en el año escolar vigente.
      Puede inscribir o reinscribir pulsando el botón "Inscribir"'''
    ENROLL_LABEL: 'Inscribir'
    ENROLLMENTS_HISTORY_LABEL: 'Histórico'
    ENROLLMENTS_HISTORY_EMPTY_MESSAGE: '''El estudiante no tiene registros
      históricos en Sofía'''
    SCORES_LABEL: 'Notas'
    CERTIFICATES_LABEL: 'Constancias'
    WITHDRAWAL_LABEL: 'Retirar'
    WITHDRAWAL_MESSAGE: 'Retirado'

angular.module 'sofia.actor'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]
