'use strict'

describe 'ActorCreateController', ->
  controller = {}
  $mdDialog = {}
  ErrorHandler = {}
  Actor = {}
  actor = id: 'irrelevant'
  STUDENT_ID = 'std'

  beforeEach ->
    module 'sofia'

    inject ($controller, _Actor_, _$mdDialog_) ->
      $mdDialog = _$mdDialog_
      spyOn $mdDialog, 'cancel'
      Actor = _Actor_
      Actor.item = actor
      controller = $controller 'ActorCreateController'

  describe 'should search person', ->
    person = id: 'irrelevant'
    actors = [
      id: STUDENT_ID, class: 'sofia.Student', person: person
    ,
      id: 'grd', class: 'sofia.Guardian', person: person
    ]
    criteria = 'not-equal'

    beforeEach ->
      controller.criteria = criteria

    afterEach ->
      expect(Actor.list).toHaveBeenCalledWith(
        onlyIdentityCard: true, criteria: criteria)

    it 'and show person data if actor found', inject ($state) ->
      spyOn $state, 'go'
      Actor.item.class = 'sofia.Teacher'
      _searchActor actors
      expect(controller.showPerson).toBeTruthy()
      expect(controller.person).toEqual(person)
      expect($state.go).not.toHaveBeenCalled()

    it 'and allow create new person if actor not found', ->
      _searchActor []
      expect(controller.showPerson).toBeFalsy()
      expect(controller.person).toEqual(identityCard: criteria)

    it 'and show actor when class is same', inject ($state) ->
      spyOn $state, 'go'
      Actor.item.class = 'sofia.Student'
      _searchActor actors
      expect($state.go).toHaveBeenCalledWith('actor', id: STUDENT_ID)
      expect($mdDialog.cancel).toHaveBeenCalled()

    _searchActor = (result) ->
      spyOn Actor, 'list'
        .and.returnValue createPromise(result)
      controller.search()

  describe 'should save new Actor', ->
    it 'and open show page', inject ($state) ->
      spyOn $state, 'go'
      spyOn Actor, 'save'
        .and.returnValue createPromise(actor)

      controller.save()
      expect(Actor.save).toHaveBeenCalledWith(actor)
      expect($state.go).toHaveBeenCalledWith('actor', id: actor.id)

    it 'or show errors', inject (ErrorHandler) ->
      error = 'irrelevant'
      spyOn ErrorHandler, 'showErrors'
      spyOn Actor, 'save'
        .and.returnValue createRejectPromise(error)

      controller.save()
      expect(ErrorHandler.showErrors).toHaveBeenCalledWith(error)

  it 'should show create dialog', ->
    spyOn $mdDialog, 'hide'
    controller.createPerson()
    expect($mdDialog.hide).toHaveBeenCalledWith true
