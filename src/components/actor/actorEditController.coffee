'use strict'

ActorEditController = (Dialog, Actor) ->
  _dialog = Dialog.create
    controller: 'ActorEditController'
    templateUrl: @sfTemplateDialog
    ds: Actor
  _actorClone = Actor.copy()
  _actorClone.person = angular.copy Actor.item.person

  class Controller

    Object.defineProperties @::,
      person: get: -> _actorClone.person
      actorClone: get: -> _actorClone
      actor: get: -> Actor.item

    cancel: _dialog.cancel
    showEditDialog: _dialog.showDialog
    openMenu: _dialog.openMenu

    save: -> _dialog.save(_actorClone)

  new Controller()

ActorEditController.$inject = ['Dialog', 'Actor']

angular.module 'sofia'
  .controller 'ActorEditController', ActorEditController
