'use strict'

describe 'ActorShowController', ->
  controller = {}
  Actor = {}
  $stateParams = id: 'irrelevant'

  beforeEach ->
    module 'sofia'

    module ($provide) ->
      $provide.value '$stateParams', $stateParams
      return

    inject ($controller, _Actor_) ->
      Actor = _Actor_
      Actor.item = id: 'irrelevant'
      spyOn Actor, 'get'
        .and.returnValue createPromise(id: 'irrelevant')

      controller = $controller 'ActorShowController'

  it 'should to load an actor from $stateParams.id', ->
    expect(Actor.get).toHaveBeenCalledWith id: $stateParams.id
    expect(controller.actor.id).toBe Actor.item.id
