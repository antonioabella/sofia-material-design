'use strict'

angular.module 'sofia.actor', []
  .constant 'ACTOR_ERRORS',
    INVALID_PERSON_FILTER: 'That option is not valid for `person` filter'
