'use strict'

describe 'personFilter', ->
  filter = {}
  Person = {}

  beforeEach ->
    module 'sofia'

    inject ($filter, _Person_) ->
      Person = _Person_
      filter = $filter 'person'

  it 'should throw an error when filter invalid', inject (ACTOR_ERRORS) ->
    error = new Error(ACTOR_ERRORS.INVALID_PERSON_FILTER)
    expect( -> filter('irrelevant', 'invalid')).toThrow(error)

  it 'should return person image', ->
    spyOn Person, 'getImage'

    person = id: 'irrelevant'
    filter(person, 'image')
    expect(Person.getImage).toHaveBeenCalledWith(person)

  it 'should return person gender', ->
    spyOn Person, 'getGender'

    person = id: 'irrelevant'
    filter(person, 'gender')
    expect(Person.getGender).toHaveBeenCalledWith(person)

  it 'should return person age', ->
    spyOn Person, 'getAge'

    person = id: 'irrelevant'
    filter(person, 'age')
    expect(Person.getAge).toHaveBeenCalledWith(person)

  it 'should return person identity card', ->
    spyOn Person, 'getIdentityCard'

    person = id: 'irrelevant'
    filter(person, 'identityCard')
    expect(Person.getIdentityCard).toHaveBeenCalledWith(person)

  it 'should return person full name', ->
    spyOn Person, 'getFullName'

    person = id: 'irrelevant'
    filter(person, 'fullName')
    expect(Person.getFullName).toHaveBeenCalledWith(person)


