'use strict'

angular.module 'sofia.actor'

.config ['$stateProvider', ($stateProvider) ->
    $stateProvider.state 'actor',
      url: '/actor/:id'
      templateUrl: 'components/actor/show.html'
      controller: 'ActorShowController'
      controllerAs: 'actorShow'
    $stateProvider.state 'actorEdit',
      url: '/actor/edit/:id'
      templateUrl: 'components/actor/edit.html'
      controller: 'ActorEditController'
      controllerAs: 'actorEdit'
]
