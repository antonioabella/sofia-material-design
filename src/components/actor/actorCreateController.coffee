'use strict'

ActorCreateController = ($state, $mdDialog, ErrorHandler, Actor) ->
  _actorClone = undefined
  _showPerson = false
  _canCreate = false
  _criteria = ''
  _checkActors = (actors) ->
    _showPerson = not _.isEmpty actors
    _canCreate = not _showPerson
  _loadPerson = (actors) ->
    Actor.item.person = _.first(actors).person if _showPerson
    Actor.item.person = identityCard: _criteria unless _showPerson
  _checkRedirect = (actors) ->
    actor = _.find actors, (a) -> a.class is Actor.item.class
    if actor?
      $mdDialog.cancel()
      $state.go 'actor', id: actor.id

  class Controller
    Object.defineProperties @::,
      person: get: -> Actor.item.person
      actor: get: -> Actor.item
      showPerson: get: -> _showPerson
      canCreate: get: -> _canCreate
      criteria:
        get: -> _criteria
        set: (value) ->
          return if _criteria is value
          _criteria = value

    search: ->
      Actor.list(onlyIdentityCard: true, criteria: _criteria).then (actors) ->
        _checkActors actors
        _loadPerson actors
        _checkRedirect actors

    cancel: $mdDialog.cancel

    save: ->
      Actor.save(Actor.item).then (actor) ->
        $mdDialog.cancel()
        $state.go 'actor', id: actor.id
      , (error) -> ErrorHandler.showErrors error

    createPerson: ->
      $mdDialog.hide(true)

  new Controller()

ActorCreateController.$inject = ['$state', '$mdDialog', 'ErrorHandler', 'Actor']

angular.module 'sofia'
  .controller 'ActorCreateController', ActorCreateController
