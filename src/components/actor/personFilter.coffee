'use strict'

personFilter = (Person, ACTOR_ERRORS) ->

  (person, type) ->
    functionName = type.charAt(0).toUpperCase() + type.slice(1)
    fun = Person["get#{functionName}"]
    return fun(person) if _.isFunction fun
    throw new Error(ACTOR_ERRORS.INVALID_PERSON_FILTER)

personFilter.$inject = ['Person', 'ACTOR_ERRORS']

angular.module 'sofia.actor'
  .filter 'person', personFilter
