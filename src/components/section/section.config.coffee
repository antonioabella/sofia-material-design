'use strict'

angular.module 'sofia.section'

.config ['$stateProvider', ($stateProvider) ->
    $stateProvider.state 'section',
      url: '/section'
      templateUrl: 'components/section/list.html'
      controller: 'SectionListController'
      controllerAs: 'sectionList'
]
