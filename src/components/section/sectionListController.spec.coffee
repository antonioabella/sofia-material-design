'use strict'

describe 'SectionListController', ->
  controller = {}
  Section = {}
  sections = (id: i, grade: position: i for i in [21..1])

  beforeEach ->
    module 'sofia'

    inject ($controller, _Section_) ->
      Section = _Section_
      spyOn Section, 'list'
        .and.returnValue createPromise(sections)
      controller = $controller 'SectionListController'

  it 'should split sections in two columns', ->
    year = id: 'irrelevant'
    controller.schoolYear = year
    expect(Section.list).toHaveBeenCalledWith(schoolYearId: year.id)
    checkGroups()

  checkGroups = ->
    groups = controller.sectionGroups
    expect(_.size groups).toBe 2
    expect(_.size groups[0]).toBe 11
    expect(_.size groups[1]).toBe 10
    expect(groups[0]).toContain id: 11, grade: position: 11
    expect(groups[0]).not.toContain id: 12, grade: position: 12
