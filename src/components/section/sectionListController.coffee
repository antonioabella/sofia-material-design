'use strict'

SectionListController = (Dialog, Section) ->
  _sectionGroups = undefined
  _schoolYearId = undefined

  _setSectionGroups = (items) ->
    size = (_.size(items) + 1) / 2
    sortedBySection = _.sortBy items, 'name'
    _sectionGroups = _.chunk (_.sortBy sortedBySection, 'grade.position'), size

  _selectYear = (schoolYearId) ->
    Section.list(schoolYearId: schoolYearId).then _setSectionGroups
    _schoolYearId = schoolYearId

  _changeList = ->
    _dialog.showDialog().then -> _selectYear _schoolYearId

  _dialog = Dialog.create
    ds: Section
    controller: 'SectionEditController'
    templateUrl: 'components/section/edit.html'

  class Controller
    Object.defineProperties @::,
      sectionGroups:
        get: -> _sectionGroups
      schoolYearId:
        get: -> _schoolYearId
        set: _selectYear

    deleteSection: (item) ->
      _dialog.deleteItem(item).then -> _setSectionGroups Section.items

    createSection: ->
      Section.create()
      Section.item.schoolYear = id: _schoolYearId
      Section.item.enrollments = []
      _changeList()

    editSection: (section) ->
      Section.select section.id
      _changeList()

  new Controller()

SectionListController.$inject = ['Dialog', 'Section']

angular.module 'sofia.section'
  .controller 'SectionListController', SectionListController
