'use strict'

translations =
  BILLING:
    BILL_LABEL: 'Facturar'
    DEBT_CONCEPTS_LABEL: 'Conceptos por Cancelar'

angular.module 'sofia.billing'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]
