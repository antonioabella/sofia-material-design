'use strict'

describe 'BillingController', ->
  controller = {}
  DebtBalance = {}
  DebtCalculator = {}
  BillingConfig = {}
  Receivable = {}
  balance = 'irrelevant'
  config = 'irrelevant'
  activeCTP = 'irrelevant'

  beforeEach ->
    module 'sofia'

    inject ($controller, _DebtBalance_, _DebtCalculator_, _Receivable_,
      _BillingConfig_) ->
      DebtCalculator = _DebtCalculator_
      Receivable = _Receivable_
      DebtBalance = _DebtBalance_
      BillingConfig = _BillingConfig_

      spyOn DebtCalculator, 'load'
      spyOn DebtCalculator, 'getActiveCTP'
        .and.returnValue activeCTP
      spyOn BillingConfig, 'load'
        .and.returnValue createPromise(config)
      spyOn DebtBalance, 'list'
        .and.returnValue createPromise(balance)
      spyOn Receivable, 'list'

      controller = $controller 'BillingController'

  describe 'should refresh balance when year change', ->
    yearId = 'irrelevant'

    it 'and solvent', ->
      Receivable.list
        .and.returnValue createPromise([])
      controller.yearId = yearId
      expect(controller.solvent).toBeTruthy()
      expect(Receivable.list).toHaveBeenCalledWith
        guardianId: undefined, schoolYearId: yearId
      expect(BillingConfig.load).toHaveBeenCalledWith yearId
      expect(DebtBalance.list).toHaveBeenCalledWith
        guardianId: undefined, schoolYearId: yearId

    it 'and not solvent', ->
      Receivable.list
        .and.returnValue createPromise(['irrelevant'])
      controller.yearId = yearId
      expect(controller.solvent).toBeFalsy()
      expect(Receivable.list).toHaveBeenCalledWith
        guardianId: undefined, schoolYearId: yearId

  it 'should save billing', inject (BillingRequest) ->
    Receivable.list
      .and.returnValue createPromise([])
    spyOn BillingRequest, 'save'
      .and.returnValue $promise: createPromise()
    controller.billingRequest()
    expect(BillingRequest.save).toHaveBeenCalledWith activeCTP
    expect(Receivable.list).toHaveBeenCalled()
