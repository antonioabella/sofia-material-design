'use strict'

BillingController = ($stateParams, DebtBalance, DebtCalculator, BillingConfig,
BillingRequest, Receivable) ->

  _solvent = false
  _yearId = undefined
  _monthId = undefined
  _actorId = $stateParams.id
  _processingRequest = false

  _refreshBalance = ->
    params = guardianId: _actorId, schoolYearId: _yearId
    Receivable.list(params).then (receivables) ->
      _solvent = _.isEmpty receivables
      if _solvent
        BillingConfig.load(_yearId).then (config) ->
          _loadBalance(config)

  _filterByMonth = ->
    DebtCalculator.filterByMonth _monthId if (_monthId? and _yearId?)

  _loadBalance = (config) ->
    DebtBalance.list(guardianId: _actorId, schoolYearId: _yearId)
      .then (balance) ->
        DebtCalculator.load balance, config
        _filterByMonth()

  _billingRequest = ->
    _processingRequest = true
    BillingRequest.save(DebtCalculator.getActiveCTP()).$promise.then ->
      _refreshBalance().then -> _processingRequest = false

  class Controller
    Object.defineProperties @::,
      yearId:
        get: -> _yearId
        set: (value) ->
          return if _yearId is value
          _yearId = value
          _refreshBalance()

      monthId:
        get: -> _monthId
        set: (value) ->
          return if _monthId is value
          _monthId = value
          _filterByMonth()

      solvent: get: -> _solvent
      actorId: get: -> _actorId
      billingRequest: get: -> _billingRequest
      processingRequest: get: -> _processingRequest

  new Controller()

BillingController.$inject = [
  '$stateParams'
  'DebtBalance'
  'DebtCalculator'
  'BillingConfig'
  'BillingRequest'
  'Receivable'
]

angular.module 'sofia.billing'
  .controller 'BillingController', BillingController
