'use strict'

angular.module 'sofia.billing'
  .config ['$stateProvider', ($stateProvider) ->
    $stateProvider.state 'billing',
      url: '/billing/:id'
      templateUrl: 'components/billing/billing.html'
      controller: 'BillingController'
      controllerAs: 'billing'
  ]
