'use strict'

translations =
  ENROLLMENT:
    LABEL: 'Inscripción'
    DATE_LABEL: 'Fecha de Inscripción'
    MONTH_LABEL: 'Mes de Inicio'
    GRADE_LABEL: 'Grado Actual'
    SECTION_LABEL: 'Sección'

angular.module 'sofia.enrollment'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]
