'use strict'

angular.module 'sofia.report'

.config ['$stateProvider', ($stateProvider) ->
    $stateProvider.state 'report',
      url: '/report'
      templateUrl: 'components/report/list.html'
      controller: 'ReportListController'
      controllerAs: 'reportList'
]
