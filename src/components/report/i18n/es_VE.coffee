'use strict'

translations =
  REPORT:
    REPORTS_LABEL: 'Reportes'
    PRINT_LABEL: 'Imprimir'
    SOLVENCE_NAME: 'Solventes/Deudores'
    SOLVENCE_DESCRIPTION: '''Detalles de estudiantes solventes o con deuda,
      agrupados por grado, para un periodo escolar, mes y año'''
    SCHOLARSHIPS_NAME: 'Estudiantes becados'
    SCHOLARSHIPS_DESCRIPTION: '''Alumnos con becas asociadas incluyendo
      padrino y monto de becas.'''
    DISCOUNTS_NAME: 'Estudiantes con descuentos de la Institución'
    DISCOUNTS_DESCRIPTION: 'Alumnos con descuento de la Institución.'
    GUARDIANS_NAME: 'Lista de representantes (firma)'
    GUARDIANS_DESCRIPTION: '''Representantes con teléfono y espacio para
      la firma.'''
    MEMO_NOT_ENROLLED_NAME: 'Comunicado estudiantes no inscritos'
    MEMO_NOT_ENROLLED_DESCRIPTION: '''Comunicado dirigido a los representantes
      que no han formalizado su inscripción a tiempo.'''
    STUDENTS_BY_GRADE_NAME: 'Estudiantes por grado'
    STUDENTS_BY_GRADE_DESCRIPTION: '''Estudiantes que integran cada grado.'''
    MEMO_DEBT_NAME: 'Comunicado Deuda'
    MEMO_DEBT_DESCRIPTION: '''Comunicado dirigido a los representantes
      con mensualidades pendientes.'''
    STUDENTS_BY_GENDER_NAME: 'Estudiantes por género'
    STUDENTS_BY_GENDER_DESCRIPTION: '''Total de estudiantes agrupados por grado,
      sección y género.'''
    STUDENTS_BY_SECTION_NAME: 'Estudiantes por sección'
    STUDENTS_BY_SECTION_DESCRIPTION: '''Alumnos agrupados por grado,
      sección y género.'''
    GUARDIANS_BY_SECTION_NAME: 'Representante por sección'
    GUARDIANS_BY_SECTION_DESCRIPTION: '''Representantes agrupados por grado,
      sección y género.'''
    DROPPED_STUDENTS_NAME: 'Alumnos retirados'
    DROPPED_STUDENTS_DESCRIPTION: 'Alumnos retirados'
    SOFIA_SUBSCRIPTION_NAME: 'Estudiantes suscritos a Sistema Sofía'
    SOFIA_SUBSCRIPTION_DESCRIPTION: 'Estudiantes suscritos a Sistema Sofía'
angular.module 'sofia.report'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]

