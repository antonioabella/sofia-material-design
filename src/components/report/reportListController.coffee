'use strict'

ReportListController = (Report) ->
  Report.list()

  class Controller
    Object.defineProperties @::,
      reports: get: -> Report.items

  new Controller()

ReportListController.$inject = ['Report']

angular.module 'sofia.report'
  .controller 'ReportListController', ReportListController
