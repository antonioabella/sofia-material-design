'use strict'

describe 'BillingPaymentDirectiveController', ->
  controller = {}
  invoices = [
    total: 10
  ,
    total: 20
  ]

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller('BillingPaymentDirectiveController',
      null, invoices: invoices)

  it 'should reject invalid paymentItem', ->
    controller.addPaymentItem()
    expect(_.size controller.paymentItems).toBe 0
    loadValidPayment()
    expect(_.size controller.paymentItems).toBe 1

  it 'should return invoices total sum', ->
    expect(controller.invoicesSum).toBe(30)

  it 'should return invoices rest by pay', ->
    loadValidPayment()
    expect(controller.paymentRest).toBe(29)

  it 'should return when rest is 0', ->
    expect(controller.restIsZero).toBeFalsy()

  it 'should remove paymentItem', ->
    loadValidPayment()
    expect(_.size controller.paymentItems).toBe 1
    controller.removePaymentItem(0)
    expect(_.size controller.paymentItems).toBe 0

  it 'should set paymentItem additional properties', ->
    checkSelectProperty(property) for property in ['bank', 'paymentMethod']

  it 'should create payment', inject ($state, Payment) ->
    spyOn Payment, 'save'
      .and.returnValue $promise: createPromise()
    spyOn $state, 'go'

    controller.createPayment()
    expect(controller.processingRequest).toBeTruthy()
    expect(Payment.save).toHaveBeenCalled()
    expect($state.go).toHaveBeenCalled()

  it 'should fail creating payment', inject ($state, Payment) ->
    spyOn Payment, 'save'
      .and.returnValue $promise: createRejectPromise()
    spyOn $state, 'go'

    controller.createPayment()
    expect(Payment.save).toHaveBeenCalled()
    expect(controller.processingRequest).toBeFalsy()

  checkSelectProperty = (property) ->
    controller["#{property}Selected"](property)
    expect(controller.paymentItem[property]).toEqual property

  loadValidPayment = ->
    controller.paymentItem.amount = 1
    controller.paymentItem.paymentMethod = 'irrelevant'
    controller.addPaymentItem()

