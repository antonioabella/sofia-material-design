'use strict'

BillingPaymentDirectiveController = ($state, Payment) ->
  _paymentItems = []
  _paymentItem = {}
  _invoices = @invoices
  _invoicesSum = 0
  _invoicesSum += i.total for i in @invoices
  _paymentItem.amount = _invoicesSum
  _paymentRest = _invoicesSum
  _calculatePaymentRest = -> _paymentRest - _paymentItem.amount
  _recalculateRest = (restitution = 0) ->
    _paymentItem.amount = restitution unless restitution is 0
    _paymentRest = _calculatePaymentRest()
    _paymentItem.amount = _paymentRest
    _paymentItem.reference = ''

  class Controller
    createPayment: ->
      @processingRequest = true
      Payment.save(_invoices, _paymentItems).$promise.then ->
        $state.go $state.current, {}, {reload: true}
      ,
        (error) => @processingRequest = false

    addPaymentItem: ->
      return unless _paymentItem.amount > 0 and _paymentItem.paymentMethod?
      _paymentItems.push _.clone _paymentItem
      _recalculateRest()

    bankSelected: (bank) -> _paymentItem.bank = bank

    paymentMethodSelected: (paymentMethod) ->
      _paymentItem.paymentMethod = paymentMethod

    removePaymentItem: (idx) ->
      item = _paymentItems.splice idx, 1
      _recalculateRest item[0].amount * -1

    Object.defineProperties @::,
      paymentItems:
        enumerable: true
        get: -> _paymentItems
      paymentItem:
        enumerable: true
        get: -> _paymentItem
      invoicesSum:
        enumerable: true
        get: -> _invoicesSum
      paymentRest:
        enumerable: true
        get: -> _paymentRest
      restIsZero:
        enumerable: true
        get: -> _paymentRest is 0

  new Controller()

BillingPaymentDirectiveController.$inject = ['$state', 'Payment']

angular.module 'sofia.components'
  .controller 'BillingPaymentDirectiveController'
    , BillingPaymentDirectiveController
