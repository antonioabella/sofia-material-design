'use strict'

BillingPaymentDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'BillingPaymentDirectiveController'
  controllerAs: 'vm'
  bindToController:
    invoices: '='
  templateUrl: 'directives/billingPayment/billingPayment.tpl.html'

angular.module 'sofia.components'
  .directive 'sfBillingPayment', BillingPaymentDirective
