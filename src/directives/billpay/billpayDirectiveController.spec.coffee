'use strict'

describe 'BillpayDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'BillpayDirectiveController'

  it 'should return payers', inject (Receivable) ->
    spyOn Receivable, 'getPayers'
    irrelevant = controller.payers
    expect(Receivable.getPayers).toHaveBeenCalled()

  it 'should return invoice link', ->
    invoice = id: 1, invoiceNumber: 1
    expect(controller.getInvoiceLink(invoice)).toContain(
      'print?id=1&number=1'
    )
