'use strict'

BillpayDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'BillpayDirectiveController'
  controllerAs: 'vm'
  bindToController: {}
  templateUrl: 'directives/billpay/billpay.tpl.html'

angular.module 'sofia.components'
  .directive 'sfBillpay', BillpayDirective
