'use strict'

BillpayDirectiveController = (Receivable, SOFIA_API) ->
  class Controller
    Object.defineProperties @::,
      payers: get: -> Receivable.getPayers()

    getInvoiceLink: (i) ->
      "#{SOFIA_API.INVOICE_PRINT_URI}?id=#{i.id}&number=#{i.invoiceNumber}"

  new Controller()

BillpayDirectiveController.$inject = ['Receivable', 'SOFIA_API']

angular.module 'sofia'
  .controller 'BillpayDirectiveController', BillpayDirectiveController
