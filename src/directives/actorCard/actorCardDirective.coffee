'use strict'

ActorCardDirective = ->
  restrict: 'E'
  scope: {}
  controller: -> return
  controllerAs: 'vm'
  bindToController:
    actor: '='
  templateUrl: 'directives/actorCard/actorCard.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorCard', ActorCardDirective
