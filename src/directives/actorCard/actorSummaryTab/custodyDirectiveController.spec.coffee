'use strict'

describe 'CustodyDirectiveController', ->
  controller = {}
  $mdDialog = {}

  beforeEach ->
    module 'sofia'

    inject ($controller, _$mdDialog_) ->
      $mdDialog = _$mdDialog_
      controller = $controller 'CustodyDirectiveController'

  it 'should set custodies when actor is set', inject (Custody) ->
    actor = 'irrelevant'
    custodies = ['irrelevant']
    spyOn Custody, 'getCustodies'
      .and.returnValue custodies
    controller.sfActor = actor
    expect(Custody.getCustodies).toHaveBeenCalledWith(actor)
    expect(controller.sfActor).toEqual actor
    expect(controller.custodies).toEqual custodies


  it 'should show searbbox', ->
    spyOn $mdDialog, 'show'

    controller.showSearchBox()
    expect($mdDialog.show).toHaveBeenCalled()

  it 'should create new custody', inject (Custody) ->
    custody = id: 'irrelevant'
    spyOn Custody, 'create'
      .and.returnValue custody
    spyOn Custody, 'save'
      .and.returnValue createPromise()
    item = 'selectedItem'
    actor = id: 'irrelevant'
    controller.sfActor = actor
    controller.selectedItem = item
    controller.create()
    expect(Custody.create).toHaveBeenCalled()
    expect(Custody.save).toHaveBeenCalledWith custody, actor
