'use strict'

ActorPersonalDirective = ->
  restrict: 'E'
  scope: {}
  controller: -> return
  controllerAs: 'vm'
  bindToController:
    actor: '='
  templateUrl:
    'directives/actorCard/actorSummaryTab/actorPersonal/actorPersonal.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorPersonal', ActorPersonalDirective
