'use strict'

ActorTaxDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorEditController'
  controllerAs: 'vm'
  bindToController:
    sfTemplateDialog: '@'
  templateUrl:
    'directives/actorCard/actorSummaryTab/actorTax/actorTax.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorTax', ActorTaxDirective
