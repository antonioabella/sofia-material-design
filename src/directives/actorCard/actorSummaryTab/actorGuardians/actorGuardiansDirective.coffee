'use strict'

_baseDir = 'directives/actorCard/actorSummaryTab'
_templateUrl = "#{_baseDir}/actorGuardians/actorGuardians.tpl.html"

ActorGuardiansDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'CustodyDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfActor: '='
  templateUrl: _templateUrl

angular.module 'sofia.components'
  .directive 'sfActorGuardians', ActorGuardiansDirective
