'use strict'

ActorWorkDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorEditController'
  controllerAs: 'vm'
  bindToController:
    sfTemplateDialog: '@'
  templateUrl:
    'directives/actorCard/actorSummaryTab/actorWork/actorWork.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorWork', ActorWorkDirective
