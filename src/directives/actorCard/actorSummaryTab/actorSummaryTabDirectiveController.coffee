'use strict'

ActorSummaryTabDirectiveCtrl = (Actor) ->
  class Controller
    isGuardian: Actor.isGuardian
    isStudent: Actor.isStudent
    isTeacher: Actor.isTeacher

  new Controller()

ActorSummaryTabDirectiveCtrl.$inject = ['Actor']

angular.module 'sofia'
  .controller 'ActorSummaryTabDirectiveController', ActorSummaryTabDirectiveCtrl
