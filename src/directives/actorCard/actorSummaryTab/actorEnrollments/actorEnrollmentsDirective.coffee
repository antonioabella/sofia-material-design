'use strict'

ActorEnrollmentsDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorEnrollmentsDirectiveController'
  controllerAs: 'vm'
  bindToController:
    actor: '='
  templateUrl: 'directives/actorCard/actorSummaryTab' +
    '/actorEnrollments/actorEnrollments.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorEnrollments', ActorEnrollmentsDirective
