'use strict'

ActorEnrollmentsDirectiveCtrl = ($mdDialog, ErrorHandler, StudentEnrollments) ->
  _enrollments = []
  _enrollment = undefined
  _enrollmentClone = undefined
  _actor = undefined
  _loadClone = ->
    if _enrollment?.id?
      _enrollmentClone = angular.copy(_enrollment)
    else
      _enrollmentClone = StudentEnrollments.create()
    _enrollmentClone.student = id: _actor.id
    _enrollmentClone.studentId = _actor.id

  class ActorEnrollmentsDirectiveController
    _this = @
    _loadEnrollmentData = (actor) ->
      StudentEnrollments.list(studentId: actor.id).then (enrollments) ->
        _enrollments = enrollments
      StudentEnrollments.get(studentId: actor.id, id: 0).then (enrollment) ->
        _enrollment = enrollment

    Object.defineProperties @::,
      enrollments: get: -> _enrollments
      enrollment: get: -> _enrollment
      enrollmentClone: get: -> _enrollmentClone
      actor:
        get: -> _actor
        set: (value) ->
          return if _actor is value
          _actor = value
          _loadEnrollmentData _actor

    cancel: $mdDialog.cancel

    gradeChanged: (grade) -> _enrollmentClone.grade = grade
    monthChanged: (month) -> _enrollmentClone.enrollmentMonth = month
    sectionChanged: (section) -> _enrollmentClone.section = section

    save: ->
      StudentEnrollments.save(_enrollmentClone).then (enrollment) ->
        _enrollment = enrollment
        $mdDialog.cancel()
      , (error) -> ErrorHandler.showErrors error

    showEnroll: (event) ->
      _loadClone()
      $mdDialog.show
        controller: _this
        controllerAs: 'vm'
        templateUrl: 'directives/actorCard/actorSummaryTab' +
          '/actorEnrollments/enroll.html'
        targetEvent: event

    openMenu: ($mdOpenMenu, $event) -> $mdOpenMenu($event)

  new ActorEnrollmentsDirectiveController()

ActorEnrollmentsDirectiveCtrl.$inject = [
  '$mdDialog'
  'ErrorHandler'
  'StudentEnrollments'
]

angular.module 'sofia'
.controller 'ActorEnrollmentsDirectiveController', ActorEnrollmentsDirectiveCtrl
