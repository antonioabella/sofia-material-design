'use strict'

describe 'ActorEnrollmentsDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'
    inject ($controller) ->
      controller = $controller 'ActorEnrollmentsDirectiveController'

  it 'should list enrollments when set actor', inject (StudentEnrollments) ->
    enrollments = ['irrelevant']
    enrollment = ['irrelevant']
    spyOn StudentEnrollments, 'list'
      .and.returnValue createPromise()
    spyOn StudentEnrollments, 'get'
      .and.returnValue createPromise()
    StudentEnrollments.items = enrollments
    StudentEnrollments.item = enrollment
    actor = id: 'irrelevant'
    controller.actor = actor
    expect(StudentEnrollments.list).toHaveBeenCalledWith studentId: actor.id
    expect(StudentEnrollments.get).toHaveBeenCalledWith(
      studentId: actor.id, id: 0)

  it 'should open menu', ->
    $event = 'irrelevant'
    $mdOpenMenu = jasmine.createSpy '$mdOpenMenu'
    controller.openMenu($mdOpenMenu, $event)
    expect($mdOpenMenu).toHaveBeenCalledWith $event

  describe 'should show enroll dialog', ->
    $mdDialog = undefined

    beforeEach ->
      inject (_$mdDialog_) ->
        $mdDialog = _$mdDialog_

      spyOn $mdDialog, 'show'
        .and.returnValue createPromise()

    it 'when enroll new', ->
      controller.showEnroll()
      expect($mdDialog.show).toHaveBeenCalled()
