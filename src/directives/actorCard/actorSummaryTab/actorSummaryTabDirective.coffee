'use strict'

ActorSummaryTabDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorSummaryTabDirectiveController'
  controllerAs: 'vm'
  bindToController:
    actor: '='
  templateUrl: 'directives/actorCard/actorSummaryTab/actorSummaryTab.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorSummaryTab', ActorSummaryTabDirective
