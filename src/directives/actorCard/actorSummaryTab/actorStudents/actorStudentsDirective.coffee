'use strict'

_baseDir = 'directives/actorCard/actorSummaryTab'
_templateUrl = "#{_baseDir}/actorStudents/actorStudents.tpl.html"

ActorStudentsDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'CustodyDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfActor: '='
  templateUrl: _templateUrl

angular.module 'sofia.components'
  .directive 'sfActorStudents', ActorStudentsDirective
