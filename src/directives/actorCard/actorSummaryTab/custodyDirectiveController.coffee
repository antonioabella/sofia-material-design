'use strict'

CustodyDirectiveController = ($mdDialog, Custody) ->
  _custodies = []
  _actor = undefined
  _selectedItem = undefined
  _childrenClass = undefined
  _kinship = 'MOTHER'
  _loadStudent = ->
    if _actor.class == 'sofia.Student' then _actor else _selectedItem
  _loadGuardian = ->
    if _actor.class == 'sofia.Student' then _selectedItem else _actor
  _loadCustody = ->
    custody = Custody.create()
    custody.student = _loadStudent()
    custody.guardian = _loadGuardian()
    custody.kinship = _kinship
    custody

  class Controller
    _this = @

    Object.defineProperties @::,
      sfActor:
        get: -> _actor
        set: (value) ->
          return if _actor is value
          _actor = value
          _custodies = Custody.getCustodies(_actor)
          _childrenClass = Custody.getChildrenClass(_actor)

      custodies: get: -> _custodies

      kinship:
        get: -> _kinship
        set: (value) ->
          return if _kinship is value
          _kinship = value

      selectedItem:
        get: -> _selectedItem
        set: (value) ->
          return if _selectedItem is value
          _selectedItem = value

      childrenClass: get: -> _childrenClass

    cancel: $mdDialog.cancel

    actorSelected: (actor) -> _selectedItem = actor

    create: ->
      Custody.save(_loadCustody(), _actor)
      $mdDialog.cancel()

    showSearchBox: (event) ->
      $mdDialog.show
        controller: _this
        controllerAs: 'vm'
        templateUrl: 'components/actor/actorCustody.html'
        parent: angular.element(document.body)
        targetEvent: event

    openMenu: ($mdOpenMenu, $event) -> $mdOpenMenu($event)

  new Controller()

CustodyDirectiveController.$inject = ['$mdDialog', 'Custody']

angular.module 'sofia'
  .controller 'CustodyDirectiveController', CustodyDirectiveController
