'use strict'

MonthsDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'MonthsDirectiveController'
  controllerAs: 'vm'
  bindToController:
    monthId: '=ngModel'
    sfExcludeEnrollment: '@'
    sfValueFromPosition: '@'
    sfLabel: '@'
    sfItemChanged: '&'

  templateUrl: 'directives/months/months.tpl.html'

angular.module 'sofia.components'
  .directive 'sfMonths', MonthsDirective
