'use strict'

describe 'MonthsDirectiveController', ->
  controller = {}
  Month = {}
  DEFAULT_POSITION = 5
  DEFAULT_ID = 3
  months = [
    id: 1, position: 3
  ,
    id: 2, position: 4
  ,
    id: DEFAULT_ID, position: DEFAULT_POSITION
  ,
    id: 4, position: 6
  ]
  defaultMonth = months[2]

  beforeEach ->
    module 'sofia'

    inject ($controller, _Month_) ->
      Month = _Month_
      Month.items = months
      spyOn Month, 'list'
      spyOn Month, 'getItemsWithoutEnrollment'
      spyOn Month, 'getDefault'
        .and.returnValue defaultMonth
      controller = $controller 'MonthsDirectiveController'

  it 'should list months on load', ->
    expect(Month.list).toHaveBeenCalled()
    expect(controller.months).toEqual(months)

  it 'should list months without enrollment month', ->
    controller.sfExcludeEnrollment = false
    controller.months
    expect(Month.getItemsWithoutEnrollment).toHaveBeenCalled()

  it 'should get default month when not month is set', ->
    monthId = controller.monthId
    expect(Month.getDefault).toHaveBeenCalled()
    expect(monthId).toEqual(DEFAULT_ID)

  it 'should set and get month property', ->
    monthId = 'irrelevant'
    controller.monthId = monthId
    expect(controller.monthId).toBe monthId

  it 'should get value from position', ->
    controller.sfValueFromPosition = false
    expect(controller.getValue(defaultMonth)).toBe DEFAULT_POSITION

  it 'should get current item', ->
    spyOn Month, 'findItem'
    controller.getCurrentItem()
    expect(Month.findItem).toHaveBeenCalled()

  it 'should get current item when sfValueFromPosition', ->
    controller.monthId = DEFAULT_ID
    controller.sfValueFromPosition = true
    expect(controller.getCurrentItem()).toEqual(months[0])
