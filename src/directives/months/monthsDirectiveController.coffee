'use strict'

MonthsDirectiveController = (Month) ->
  _monthId = undefined
  Month.list()

  class Controller
    Object.defineProperties @::,
      months:
        get: ->
          return Month.getItemsWithoutEnrollment() if @sfExcludeEnrollment?
          Month.items
      monthId:
        get: -> _monthId || @getValue(Month.getDefault())
        set: (value) ->  _monthId = value unless _monthId is value

    getCurrentItem: ->
      id = _.parseInt _monthId
      if @sfValueFromPosition?
        _.find Month.items, (m) -> m.position is id
      else
        Month.findItem(id)

    getValue: (m) -> if @sfValueFromPosition? then m?.position else m?.id

  new Controller()

MonthsDirectiveController.$inject = ['Month']

angular.module 'sofia.components'
  .controller 'MonthsDirectiveController', MonthsDirectiveController
