'use strict'

ActorLabelDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorLabelDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfActorId: '@'
  templateUrl: 'directives/actorLabel/actorLabel.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorLabel', ActorLabelDirective
