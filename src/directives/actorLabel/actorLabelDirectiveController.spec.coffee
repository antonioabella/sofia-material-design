'use strict'

describe 'ActorLabelDirectiveController', ->
  controller = {}
  Actor = {}

  beforeEach ->
    module 'sofia'

    inject ($controller, _Actor_) ->
      controller = $controller 'ActorLabelDirectiveController'
      Actor = _Actor_

  it 'should return actor', ->
    item = createActor()
    expect(controller.actor).toBe(item)

  createActor = ->
    item = {peson: 'irrelevant'}
    Actor.item = item
    item
