'use strict'
ActorLabelDirectiveController = (Actor) ->
  Actor.get id: @sfActorId

  class Controller

    Object.defineProperties @::,
      actor: get: -> Actor.item

  new Controller()

ActorLabelDirectiveController.$inject = ['Actor']

angular.module 'sofia.components'
  .controller 'ActorLabelDirectiveController', ActorLabelDirectiveController
