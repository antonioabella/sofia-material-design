'use strict'

GenericSelectDirectiveController = ($translate, SOFIA_API, DataService) ->
  _id = 0
  _key = _.snakeCase(@sfType).toUpperCase()
  @sfLabel = $translate.instant "GENERAL.#{_key}_LABEL" unless @sfLabel?
  _label = @sfLabel

  _ds = new DataService(url: SOFIA_API["#{_key}_URI"])
  _ds.list()

  class Controller
    Object.defineProperties @::,
      id:
        get: -> _id
        set: (value) -> _id = value unless _id is value
      label: get: -> _label
      items: get: -> _ds.items

    getSelectedItem: ->
      kid = _.parseInt(_id)
      kid = if _.isNaN(kid) then _id else kid
      _ds.findItem kid

  new Controller()

GenericSelectDirectiveController
  .$inject = ['$translate', 'SOFIA_API', 'DataService']

angular.module 'sofia.components'
  .controller 'GenericSelectDirectiveController'
  , GenericSelectDirectiveController
