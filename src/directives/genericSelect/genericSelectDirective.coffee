'use strict'

GenericSelectDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'GenericSelectDirectiveController'
  controllerAs: 'vm'
  bindToController:
    id: '=ngModel'
    disabled: '=ngDisabled'
    sfLabel: '@'
    sfType: '@'
    sfItemChanged: '&'
  templateUrl: (elem, attrs) ->
    attrs.templateUrl || 'directives/genericSelect/genericSelect.tpl.html'

angular.module 'sofia.components'
  .directive 'sfGenericSelect', GenericSelectDirective
