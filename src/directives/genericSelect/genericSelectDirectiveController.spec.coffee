'use strict'

describe 'GenericSelectDirectiveController', ->
  api = {}
  argsCollector = {}
  DataService = jasmine.createSpyObj 'DataService', ['list', 'findItem']
  type = 'irrelevantWord'
  key = 'IRRELEVANT_WORD'
  uri = 'irrelevantUri'

  beforeEach ->
    module 'sofia'

    DataServiceInstance = (args) ->
      argsCollector = args
      DataService

    module ($provide) ->
      $provide.value 'DataService', DataServiceInstance
      return

    inject (SOFIA_API, _DataService_) ->
      api = SOFIA_API
      api["#{key}_URI"] = uri

  it 'should set sfLabel when is null', inject ($controller, $translate) ->
    label = 'GENERAL.IRRELEVANT_WORD_LABEL'
    spyOn $translate, 'instant'
      .and.returnValue label

    controller = $controller 'GenericSelectDirectiveController', null, {
      sfType: 'irrelevantWord'
    }
    expect($translate.instant).toHaveBeenCalledWith label
    expect(controller.label).toEqual label

  it 'should get item list', inject ($controller) ->
    controller = $controller 'GenericSelectDirectiveController', null, {
      sfType: 'irrelevantWord'
    }
    expect(argsCollector).toEqual(url: uri)
    expect(DataService.list).toHaveBeenCalled()

  it 'should find item', inject ($controller) ->
    controller = $controller 'GenericSelectDirectiveController'
    checkSelected controller, '44', 44
    checkSelected controller, 'irrelevant', 'irrelevant'

  checkSelected = (controller, id, calledWith) ->
    controller.id = id
    controller.getSelectedItem()
    expect(DataService.findItem).toHaveBeenCalledWith(calledWith)
