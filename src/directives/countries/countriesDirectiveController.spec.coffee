'use strict'

describe 'CountriesDirectiveController', ->
  controller = {}
  Country = {}
  countries = [id: 'VE', name: 'Venezuela']

  beforeEach ->
    module 'sofia'

    inject ($controller, _Country_) ->
      Country = _Country_
      spyOn Country, 'list'
        .and.returnValue createPromise()
      Country.items = countries
      controller = $controller 'CountriesDirectiveController'
      expect(Country.list).toHaveBeenCalled()

  it 'should return countries', ->
    expect(controller.countries).toEqual(countries)

  it 'should handle country property', ->
    expect(controller.id).toBe(countries[0].id)
    controller.id = 'VE'
    expect(controller.id).toBe('VE')
    controller.id = 'HO'
    expect(controller.id).toBe('HO')
