'use strict'

CountriesDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'CountriesDirectiveController'
  controllerAs: 'vm'
  bindToController:
    id: '=ngModel'
    sfLabel: '@'
  templateUrl: 'directives/countries/countries.tpl.html'

angular.module 'sofia.components'
  .directive 'sfCountries', CountriesDirective
