'use strict'

CountriesDirectiveController = (Country) ->
  _id = undefined
  Country.list().then -> _id = Country.getDefault(Country.items)?.id

  class Controller
    Object.defineProperties @::,
      countries: get: -> Country.items
      id:
        get: -> _id
        set: (value) -> _id = value unless value is _id

  new Controller()

CountriesDirectiveController.$inject = ['Country']

angular.module 'sofia'
  .controller 'CountriesDirectiveController', CountriesDirectiveController
