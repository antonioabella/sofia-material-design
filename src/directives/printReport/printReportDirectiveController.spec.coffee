'use strict'

describe 'PrintReportDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'PrintReportDirectiveController'

  it 'should show parameter builder dialog', inject ($mdDialog) ->
    spyOn $mdDialog, 'show'
    spyOn angular, 'element'
      .and.returnValue ''

    report = template: 'irrelevant'
    event = 'irrelevant'

    showObj =
      controller: 'ReportParameterBuilderController'
      controllerAs: 'vm'
      templateUrl: 'directives/printReport/templates/irrelevant.html'
      targetEvent: event
      parent: ''
      locals:
        report: report

    controller.showParameterBuilder(report, event)
    expect(angular.element).toHaveBeenCalled()
    expect($mdDialog.show).toHaveBeenCalledWith(showObj)
