'use strict'

describe 'ReportParameterBuilderCtrl', ->
  controller = {}
  $mdDialog = {}
  report = id: 'irrelevant'

  beforeEach ->
    module 'sofia'

    module ($provide) ->
      $provide.value 'report', report
      return

    inject ($controller, _$mdDialog_) ->
      $mdDialog = _$mdDialog_
      controller = $controller 'ReportParameterBuilderController'

  it 'should select parameters to print report', inject (SOFIA_API) ->
    url = "#{SOFIA_API.REPORT_PRINT_URI}/irrelevant?isDebt=1&conceptId=1"
    expect(controller.getReportLink(controller.builder)).toBe url
