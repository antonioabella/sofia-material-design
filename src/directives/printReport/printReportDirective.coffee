'use strict'

PrintReportDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'PrintReportDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfReport: '='
  templateUrl: 'directives/printReport/printReport.tpl.html'

angular.module 'sofia.components'
  .directive 'sfPrintReport', PrintReportDirective
