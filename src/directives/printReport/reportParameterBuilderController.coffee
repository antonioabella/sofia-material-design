'use strict'

ReportParameterBuilderCtrl = ($mdDialog, SOFIA_API, report) ->
  _getReportLink = (params) ->
    paramsArray = for k, v of params
      "#{k}=#{v}"

    "#{SOFIA_API.REPORT_PRINT_URI}/#{report.id}?#{paramsArray.join('&')}"

  class Controller
    builder: isDebt: 1, conceptId: 1
    cancel: $mdDialog.cancel
    getReportLink: -> _getReportLink(@builder)

  new Controller()

ReportParameterBuilderCtrl.$inject = ['$mdDialog', 'SOFIA_API', 'report']

angular.module 'sofia.report'
  .controller 'ReportParameterBuilderController', ReportParameterBuilderCtrl
