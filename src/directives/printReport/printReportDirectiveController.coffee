'use strict'

PrintReportDirectiveController = ($mdDialog) ->
  @showParameterBuilder = (report, event) -> $mdDialog.show
    controller: 'ReportParameterBuilderController'
    controllerAs: 'vm'
    templateUrl: "directives/printReport/templates/#{report.template}.html"
    targetEvent: event
    parent: angular.element(document.body)
    locals:
      report: report

  @

PrintReportDirectiveController.$inject = ['$mdDialog']

angular.module 'sofia.components'
  .controller 'PrintReportDirectiveController', PrintReportDirectiveController
