'use strict'

describe 'ChecklistDirectiveController', ->
  controller = {}
  TOTAL_ITEMS = 25
  items = (id: i for i in [1..TOTAL_ITEMS])

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'ChecklistDirectiveController'
      controller.items = items

  it 'should check and uncheck all elements', ->
    controller.checkAll()
    expect(_.size _.filter(controller.items, 'checked')).toBe TOTAL_ITEMS
    controller.uncheckAll()
    expect(_.size _.filter(controller.items, 'checked')).toBe 0
