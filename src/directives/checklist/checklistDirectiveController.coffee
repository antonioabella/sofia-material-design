'use strict'

ChecklistDirectiveController = ->
  _setAll = (value) => i.checked = value for i in @items

  @checkAll = -> _setAll true
  @uncheckAll = -> _setAll false

  @

ChecklistDirectiveController.$inject = []

angular.module 'sofia.components'
  .controller 'ChecklistDirectiveController', ChecklistDirectiveController
