'use strict'

ChecklistDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ChecklistDirectiveController'
  controllerAs: 'vm'
  bindToController:
    items: '=ngModel'
    sfLabel: '@'
    sfGetItemText: '&'
  templateUrl: 'directives/checklist/checklist.tpl.html'

angular.module 'sofia.components'
  .directive 'sfChecklist', ChecklistDirective
