'use strict'

IconListItemDirectiveController = ->
  @getIconName = (iconName) -> "images/svg/#{iconName}.svg"
  @

IconListItemDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'IconListItemDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfIconName: '@'
    sfItemText: '@'
  templateUrl: 'directives/iconListItem/iconListItem.tpl.html'

angular.module 'sofia.components'
  .controller 'IconListItemDirectiveController', IconListItemDirectiveController
  .directive 'sfIconListItem', IconListItemDirective
