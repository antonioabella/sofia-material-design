'use strict'

describe 'IconListItemDirectiveController', ->
  beforeEach ->
    module 'sofia.components'

  it 'should return iconName', inject ($controller) ->
    controller = $controller 'IconListItemDirectiveController'
    expect(controller.getIconName('irrelevant')).toEqual(
      'images/svg/irrelevant.svg')
