'use strict'

describe 'PrintInvoiceDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'PrintInvoiceDirectiveController'

  it 'return url to print invoice', inject (SOFIA_API) ->
    i = id: 1, invoiceNumber: 1
    url = "#{SOFIA_API.INVOICE_PRINT_URI}/1?name=1"
    expect(controller.getInvoiceLink(i)).toEqual(url)
