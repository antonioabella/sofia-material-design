'use strict'

PrintInvoiceDirectiveController = (SOFIA_API) ->
  class Controller
    getInvoiceLink: (i) ->
      "#{SOFIA_API.INVOICE_PRINT_URI}/#{i.id}?name=#{i.invoiceNumber}"

  new Controller()

PrintInvoiceDirectiveController.$inject = ['SOFIA_API']

angular.module 'sofia.components'
  .controller 'PrintInvoiceDirectiveController', PrintInvoiceDirectiveController
