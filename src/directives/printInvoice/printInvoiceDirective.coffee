'use strict'

PrintInvoiceDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'PrintInvoiceDirectiveController'
  controllerAs: 'vm'
  bindToController:
    invoice: '='
  templateUrl: 'directives/printInvoice/printInvoice.tpl.html'

angular.module 'sofia.components'
  .directive 'sfPrintInvoice', PrintInvoiceDirective
