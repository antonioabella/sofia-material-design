'use strict'

SchoolYearsDirectiveController = (SchoolYear) ->
  _years = []
  _yearsPlus = []
  _id = undefined

  class Controller
    SchoolYear.list().then (data) ->
      _years = data
      _yearsPlus = _.union _years, [{id: 0, name: 'Show All'}]
      _id = SchoolYear.getDefault()?.id

    Object.defineProperties @::,
      schoolYears:
        get: -> if @sfShowAll? then _yearsPlus else _years
      id:
        get: -> _id
        set: (value) -> _id = value unless _id is value

    yearName: SchoolYear.getName

    getCurrentItem: -> SchoolYear.findItem(_.parseInt _id)

  new Controller()

SchoolYearsDirectiveController.$inject = ['SchoolYear']

angular.module 'sofia.components'
  .controller 'SchoolYearsDirectiveController', SchoolYearsDirectiveController
