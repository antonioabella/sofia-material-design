'use strict'

SchoolYearsDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'SchoolYearsDirectiveController'
  controllerAs: 'vm'
  bindToController:
    id: '=ngModel'
    sfShowAll: '@'
    sfLabel: '@'
    sfItemChanged: '&'
  templateUrl: 'directives/schoolYears/schoolYears.tpl.html'

angular.module 'sofia.components'
  .directive 'sfSchoolYears', SchoolYearsDirective
