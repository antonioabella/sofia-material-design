'use strict'

describe 'SchoolYearsDirectiveController', ->
  controller = {}
  SchoolYear = {}
  years = [id: 1, name: 'irrelevant']

  beforeEach ->
    module 'sofia'

    inject ($controller, _SchoolYear_) ->
      SchoolYear = _SchoolYear_
      spyOn SchoolYear, 'list'
        .and.returnValue(createPromise(years))
      spyOn SchoolYear, 'getDefault'
        .and.returnValue(years[0])

      controller = $controller 'SchoolYearsDirectiveController'
      expect(SchoolYear.list).toHaveBeenCalled()

  it 'should return school years', ->
    expect(controller.schoolYears).toEqual(years)
    expect(_.size controller.schoolYears).toBe 1
    controller.sfShowAll = true
    expect(_.size controller.schoolYears).toBe 2

  it 'should handle id property', ->
    expect(controller.id).toBe(years[0].id)
    controller.id = 99
    expect(controller.id).toBe(99)
    controller.id = 99
    expect(controller.id).toBe(99)

  it 'should find current item', ->
    spyOn SchoolYear, 'findItem'
    id = '1'
    controller.id = id
    controller.getCurrentItem()
    expect(SchoolYear.findItem).toHaveBeenCalledWith _.parseInt id


