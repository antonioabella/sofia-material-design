'use strict'

DebtDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'DebtDirectiveController'
  controllerAs: 'vm'
  templateUrl: 'directives/debt/debt.tpl.html'

angular.module 'sofia.components'
  .directive 'sfDebt', DebtDirective
