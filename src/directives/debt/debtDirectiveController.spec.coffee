'use strict'

describe 'DebtDirectiveController', ->
  controller = {}
  DebtCalculator = {}

  beforeEach ->
    module 'sofia'

    inject ($controller, _DebtCalculator_) ->
      DebtCalculator = _DebtCalculator_

      controller = $controller 'DebtDirectiveController'

  it 'should delegete load to DebtCalculator', ->
    expect(controller.getConceptsToPay).toBe(DebtCalculator.getConceptsToPay)

