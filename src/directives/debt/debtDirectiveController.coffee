'use strict'

DebtDirectiveController = (DebtCalculator) ->
  class Controller
    getEnrollments: DebtCalculator.getEnrollments
    getConcepts: DebtCalculator.getConcepts
    filterCTP: DebtCalculator.filterCTP
    monthName: DebtCalculator.getShortMonthName
    calculateTotal: DebtCalculator.calculateTotal
    switchEnrollment: DebtCalculator.switchEnrollment

  new Controller

DebtDirectiveController.$inject = ['DebtCalculator']

angular.module 'sofia.components'
  .controller 'DebtDirectiveController', DebtDirectiveController
