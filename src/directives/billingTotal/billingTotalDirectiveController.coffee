'use strict'

BillingTotalDirectiveController = (DebtCalculator) ->
  class Controller
    Object.defineProperties @::,
      total: get: -> DebtCalculator.total
      subtotal: get: -> DebtCalculator.subtotal
      discount: get: -> DebtCalculator.discount

  new Controller()

BillingTotalDirectiveController.$inject = ['DebtCalculator']

angular.module 'sofia.components'
  .controller 'BillingTotalDirectiveController', BillingTotalDirectiveController
