'use strict'

BillingTotalDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'BillingTotalDirectiveController'
  controllerAs: 'vm'
  bindToController: {}
  templateUrl: 'directives/billingTotal/billingTotal.tpl.html'

angular.module 'sofia.components'
  .directive 'sfBillingTotal', BillingTotalDirective
