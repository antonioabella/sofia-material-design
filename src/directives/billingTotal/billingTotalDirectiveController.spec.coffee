'use strict'

describe 'BillingTotalDirectiveController', ->
  controller = {}
  DebtCalculator = {}

  beforeEach ->
    module 'sofia'

    inject ($controller, _DebtCalculator_) ->
      DebtCalculator = _DebtCalculator_
      controller = $controller 'BillingTotalDirectiveController'

  it 'should return debt total', ->
    expect(controller.total).toEqual(DebtCalculator.total)

  it 'should return debt subtotal', ->
    expect(controller.subtotal).toEqual(DebtCalculator.subtotal)

  it 'should return debt discount', ->
    expect(controller.discount).toEqual(DebtCalculator.discount)
