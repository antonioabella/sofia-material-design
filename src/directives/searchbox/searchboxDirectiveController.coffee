'use strict'

searchboxDirectiveController = (Actor, Person) ->
  _searchType = @sfSearchType
  _type = @sfActorType

  class Controller
    querySearch: (query) ->
      Actor.list(criteria: query, searchType: _searchType, actorType: _type)
    getFullName: Person.getFullName

  new Controller()

searchboxDirectiveController.$inject = ['Actor', 'Person']

angular.module 'sofia.components'
  .controller 'searchboxDirectiveController', searchboxDirectiveController
