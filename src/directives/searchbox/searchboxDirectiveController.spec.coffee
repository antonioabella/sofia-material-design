'use strict'

describe 'searchboxDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'searchboxDirectiveController'

  it 'should search actor', inject (Actor) ->
    spyOn Actor, 'list'

    query = 'irrelevant'
    controller.querySearch(query)
    expect(Actor.list).toHaveBeenCalledWith(
      criteria: query, searchType: Actor.SEARCH_ACTIVE)
