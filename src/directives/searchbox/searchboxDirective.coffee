'use strict'

SearchboxDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'searchboxDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfSelectedItemChange: '&'
    sfSearchType: '@'
    sfActorType: '@'
    sfSelectedItem: '='
  templateUrl: 'directives/searchbox/searchbox.tpl.html'

angular.module 'sofia.components'
  .directive 'sfSearchbox', SearchboxDirective
