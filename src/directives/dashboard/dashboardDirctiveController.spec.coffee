'use strict'

describe 'DashboardDirectiveController', ->
  controller = undefined
  $mdDialog = undefined

  beforeEach ->
    module 'sofia'

    inject ($controller, _$mdDialog_) ->
      $mdDialog = _$mdDialog_
      controller = $controller 'DashboardDirectiveController'

  it 'should send to actor page when actor selected', inject ($state) ->
    actor = id: 'irrelevant'
    spyOn $state, 'go'

    controller.actorSelected actor
    expect($state.go).toHaveBeenCalledWith('actor', id: actor.id)

  it 'should open dialog to create actor', ->
    spyOn $mdDialog, 'show'

    actorType = 'irrelevant'
    controller.createActor(actorType)
    expect($mdDialog.show).toHaveBeenCalled()
