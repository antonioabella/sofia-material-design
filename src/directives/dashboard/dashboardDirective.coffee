'use strict'

DashboardDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'DashboardDirectiveController'
  controllerAs: 'vm'
  bindToController: {}
  templateUrl: 'directives/dashboard/dashboard.tpl.html'

angular.module 'sofia.components'
  .directive 'sfDashboard', DashboardDirective
