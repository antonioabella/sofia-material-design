'use strict'

DashboardDirectiveController = ($mdDialog, $state, Actor) ->
  class Controller
    actorSelected: (actor) ->
      $state.go 'actor', id: actor.id

    createActor: (actorType, event) ->
      Actor.create()
      Actor.item.class = actorType
      $mdDialog.show
        controller: 'ActorCreateController'
        controllerAs: 'vm'
        templateUrl: 'components/actor/create.html'
        parent: angular.element(document.body)
        targetEvent: event
      .then (answer) ->
        if answer
          $mdDialog.show
            controller: 'ActorCreateController'
            controllerAs: 'vm'
            templateUrl: 'components/actor/edit.html'
            parent: angular.element(document.body)
            targetEvent: event

  new Controller()

DashboardDirectiveController.$inject = ['$mdDialog', '$state', 'Actor']

angular.module 'sofia.components'
  .controller 'DashboardDirectiveController', DashboardDirectiveController
