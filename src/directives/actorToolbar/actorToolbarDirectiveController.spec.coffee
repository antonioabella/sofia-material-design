'use strict'

describe 'ActorToolbarDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'ActorToolbarDirectiveController'

  it 'should open edit dialog', inject ($mdDialog) ->
    spyOn $mdDialog, 'show'
    controller.showEdit()
    expect($mdDialog.show).toHaveBeenCalled()
