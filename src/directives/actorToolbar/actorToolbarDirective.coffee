'use strict'

ActorToolbarDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'ActorToolbarDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfActor: '='
  templateUrl: 'directives/actorToolbar/actorToolbar.tpl.html'

angular.module 'sofia.components'
  .directive 'sfActorToolbar', ActorToolbarDirective
