'use strict'

ActorToolbarDirectiveController = ($mdDialog) ->
  @showEdit = (event) -> $mdDialog.show
    controller: 'ActorEditController'
    controllerAs: 'vm'
    templateUrl: 'components/actor/edit.html'
    parent: angular.element(document.body)
    targetEvent: event

  @

ActorToolbarDirectiveController.$inject = ['$mdDialog']

angular.module 'sofia.components'
  .controller 'ActorToolbarDirectiveController', ActorToolbarDirectiveController
