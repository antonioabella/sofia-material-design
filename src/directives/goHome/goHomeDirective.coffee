'use strict'

GoHomeDirective = ->
  restrict: 'E'
  scope: {}
  controller: -> return
  controllerAs: 'vm'
  bindToController:
    sfTitle: '@'
  templateUrl: 'directives/goHome/goHome.tpl.html'

angular.module 'sofia.components'
  .directive 'sfGoHome', GoHomeDirective
