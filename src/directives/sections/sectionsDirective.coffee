'use strict'

SectionsDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'GenericSelectDirectiveController'
  controllerAs: 'vm'
  bindToController:
    id: '=ngModel'
    sfGrade: '='
    sfLabel: '@'
    sfType: '@'
    sfItemChanged: '&'

  templateUrl: 'directives/sections/sections.tpl.html'

angular.module 'sofia.components'
  .directive 'sfSections', SectionsDirective
