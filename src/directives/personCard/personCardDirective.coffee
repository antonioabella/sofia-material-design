'use strict'

PersonCardDirective = ->
  restrict: 'E'
  scope: {}
  controller: -> return
  controllerAs: 'vm'
  bindToController:
    sfPerson: '='
  templateUrl: 'directives/personCard/personCard.tpl.html'

angular.module 'sofia.components'
  .directive 'sfPersonCard', PersonCardDirective
