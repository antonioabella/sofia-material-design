'use strict'

describe 'BillingButtonDirectiveController', ->
  controller = {}

  beforeEach ->
    module 'sofia'

    inject ($controller) ->
      controller = $controller 'BillingButtonDirectiveController'

  it 'should return main guardian id for actor', inject (ActorGuardian) ->
    guardianId = 'irrelevant'
    items = [id: guardianId]
    ActorGuardian.items = items
    spyOn ActorGuardian, 'list'
      .and.returnValue createPromise(items)
    actor = id: 'irrelevant'
    controller.sfActor = actor
    expect(ActorGuardian.list).toHaveBeenCalled()
    expect(controller.guardianId).toBe guardianId
    expect(controller.sfActor).toEqual actor
