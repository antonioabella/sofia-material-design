'use strict'

BillingButtonDirective = ->
  restrict: 'E'
  scope: {}
  controller: 'BillingButtonDirectiveController'
  controllerAs: 'vm'
  bindToController:
    sfActor: '='
  templateUrl: 'directives/billingButton/billingButton.tpl.html'


angular.module 'sofia.components'
  .directive 'sfBillingButton', BillingButtonDirective
