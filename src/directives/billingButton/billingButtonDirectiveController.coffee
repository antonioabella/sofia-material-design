'use strict'

BillingButtonDirectiveCtrl = (ActorGuardian) ->
  _actor = undefined
  _guardianId = undefined

  class BillingButton
    Object.defineProperties @::,
      sfActor:
        get: -> _actor
        set: (value) ->
          return if _actor is value
          _actor = value
          ActorGuardian.list(actorId: _actor.id, mainOnly: true).then ->
            _guardianId = ActorGuardian.items[0]?.id
      guardianId: get: -> _guardianId

  new BillingButton()

BillingButtonDirectiveCtrl.$inject = ['ActorGuardian']

angular.module 'sofia.components'
  .controller 'BillingButtonDirectiveController', BillingButtonDirectiveCtrl
