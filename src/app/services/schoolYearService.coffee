'use strict'

SchoolYearService = ($cacheFactory, DataService, SOFIA_API) ->
  _actions = query: {method: 'GET', cache: $cacheFactory, isArray: true}

  class SchoolYear extends DataService
    _getYear = (date) -> new Date(date).getFullYear()
    _getActive = -> _.find @items, (sy) -> sy.active
    _getEnrollment = -> _.find @items, (sy) -> sy.enrollment
    _getName = (schoolYear) ->
      return schoolYear.name if schoolYear.name?
      "#{_getYear(schoolYear.start)} - #{_getYear(schoolYear.end)}"

    getDefault: _getActive
    getActive: _getActive
    getEnrollment: _getEnrollment
    getName: _getName
    toString: -> 'SchoolYear'

  new SchoolYear(url: SOFIA_API.SCHOOL_YEAR_URI, actions: _actions)

SchoolYearService.$inject = ['$cacheFactory', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'SchoolYear', SchoolYearService
