'use strict'

SectionService = (DataService, SOFIA_API) ->
  actions =
    query: {method: 'GET', isArray: true}
    update: method: 'PUT'
  params = id: '@id'
  url = SOFIA_API.SECTION_URI

  _cleanEnrollments = (enrollments) ->
    _.map (_.pluck enrollments, 'id'), (i) -> id: i

  class Section extends DataService
    save: (section) ->
      enrollments = _cleanEnrollments(section.enrollments)
      if section.id?
        id = section.id
        @resource.update({id, section, enrollments}).$promise.then @_updateItems
      else
        @resource.save({section, enrollments}).$promise.then @_updateItems

  new Section({url, actions, params})

SectionService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Section', SectionService
