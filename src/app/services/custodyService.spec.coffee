'use strict'

describe 'CustodyService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (Custody) ->
      service = Custody

  it 'should get custodians from student', ->
    actor = getJSONFixture('student.json')
    custodies = service.getCustodies(actor)
    expect(custodies.length).toEqual(2)
    checkActors custodies, [3443, 3444]

  it 'should get custodianships from guardian', ->
    actor = getJSONFixture('guardian.json')
    custodies = service.getCustodies(actor)
    expect(custodies.length).toEqual(2)
    checkActors custodies, [585, 2954]

  checkActors = (custodies, actorIds) ->
    for id in actorIds
      expect(_.find custodies, 'actorId', id).toBeTruthy()
