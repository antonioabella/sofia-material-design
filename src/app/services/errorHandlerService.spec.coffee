'use strict'

describe 'ErrorHandlerService', ->
  service = {}
  $mdToast = {}
  simpleToast = jasmine.createSpyObj('simpleToast', ['content'])

  beforeEach ->
    module 'sofia'

    inject (_$mdToast_, ErrorHandler) ->
      $mdToast = _$mdToast_
      spyOn $mdToast, 'show'
      spyOn $mdToast, 'simple'
        .and.returnValue simpleToast

      service = ErrorHandler

  it 'should show message', ->
    msg = 'irrelevant'
    service.showMessage(msg)
    expect($mdToast.show).toHaveBeenCalled()
    expect(simpleToast.content).toHaveBeenCalledWith(msg)

  it 'should show api errors', ->
    errors = data: errors: [
      message: 'irrelevant'
    ,
      message: 'irrelevant'
    ]
    msg = 'irrelevant\nirrelevant'
    service.showErrors(errors)
    expect($mdToast.show).toHaveBeenCalled()
    expect(simpleToast.content).toHaveBeenCalledWith(msg)
