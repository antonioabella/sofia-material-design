'use strict'

CountryService = ($locale, DataService, SOFIA_API) ->
  class Country extends DataService
    getDefault: (countries) ->
      id = $locale.id.slice(-2).toUpperCase()
      _.find countries, (c) -> c.id is id

  new Country(url: SOFIA_API.COUNTRY_URI)

CountryService.$inject = ['$locale', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Country', CountryService
