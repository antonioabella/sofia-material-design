'use strict'

DCS = (Discount, Cost, Household, ConceptToPayCalculator, BillingMonth) ->
  [_balance, _billingConfig, _billingMonths] = [{}, {}, {}]
  [_debtConcepts, _conceptsToPay, _enrollments, _activeCTP] = [[],[],[],[]]
  [_total, _discount, _subtotal] = [0, 0, 0]
  CALCULATORS = ['MONTHLY', 'ENROLLMENT', 'APPOINTED']

  _calculateDebtConcepts = ->
    d = new Discount(_balance)
    c = new Cost(_billingConfig.conceptCosts, CALCULATORS)
    h = new Household(_balance, _billingConfig.concepts)
    ctp = new ConceptToPayCalculator(_balance, _billingMonths)
    _.compose(d.calculate, c.calculate, h.filter, ctp.build)()

  _enrollmentAndConcept = (enrollmentId, conceptId) ->
    (ctp) -> ctp.enrollmentId is enrollmentId and ctp.conceptId is conceptId

  _getConceptsToPay = (monthId) ->
    monthPosition = _billingMonths.getPosition(monthId)
    _.filter _debtConcepts, (ctp) ->
      ctp.monthPosition <= monthPosition or
        ctp.monthPosition is _billingMonths.getInitialMonth().position

  _getEnrollments = ->
    _.filter _balance.enrollments, (e) ->
      _.some _conceptsToPay, (ctp) -> ctp.enrollmentId is e.id

  _getActiveEnrollments = ->
    _.map _getEnrollments(), (e) -> _.extend e, active: true

  _getConcepts = (enrollmentId) ->
    _.filter _billingConfig.concepts, (c) ->
      _.some _conceptsToPay, _enrollmentAndConcept(enrollmentId, c.id)

  _calculateTotal = ->
    _activeCTP = _.filter _conceptsToPay, (ctp) -> ctp.active
    _total = _.sum _activeCTP, 'amount'
    _discount = _.sum _activeCTP, 'discount'
    _subtotal = _total + _discount

  _filterCTP = (enrollmentId, conceptId) ->
    _.filter _conceptsToPay, _enrollmentAndConcept(enrollmentId, conceptId)

  _swithEnrollment = (enrollmentId) ->
    enrollment = _.find _enrollments, (e) -> e.id is enrollmentId
    for c in _conceptsToPay
      c.active = enrollment.active if c.enrollmentId is enrollmentId
    _calculateTotal()

  class DebtCalculator
    load: (balance, billingConfig) ->
      _billingConfig = billingConfig
      _balance = balance
      _billingMonths = new BillingMonth(billingConfig.months)
      _debtConcepts = _calculateDebtConcepts()

    filterByMonth: (monthId) ->
      return unless _billingConfig.months?
      _conceptsToPay = _getConceptsToPay(monthId)
      _enrollments = _getActiveEnrollments()
      _calculateTotal()

    getEnrollments: -> _enrollments
    getConcepts: _getConcepts
    filterCTP: _filterCTP
    getShortMonthName: (monthId) -> _billingMonths.getShortName(monthId)
    calculateTotal: _calculateTotal
    switchEnrollment: _swithEnrollment
    getActiveCTP: -> _activeCTP
    Object.defineProperties @::,
      total:
        enumerable: true
        get: -> _total
      discount:
        enumerable: true
        get: -> _discount
      subtotal:
        enumerable: true
        get: -> _subtotal
  new DebtCalculator()

DCS.$inject = ['Discount', 'Cost', 'Household', 'ConceptToPayCalculator',
  'BillingMonth'
]

angular.module 'sofia'
  .factory 'DebtCalculator', DCS
