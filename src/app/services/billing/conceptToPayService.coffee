'use strict'

ConceptToPayService = ->
  class ConceptToPay
    constructor: (@enrollmentId, @conceptId, @monthId, @monthPosition,
      @amount = 0, @discount = 0, @active = true) -> return

  ConceptToPay

angular.module 'sofia'
  .factory 'ConceptToPay', ConceptToPayService
