'use strict'

describe 'BillingMonthService', ->
  service = {}
  months = []

  beforeEach ->
    module 'sofia'

    months = FixturesLoader.months()

    inject (BillingMonth) -> service = new BillingMonth(months)

  it 'should return month by id', ->
    expect(service.get(1).position).toBe 5
    expect(service.get(9).position).toBe 1
    expect(service.get(8).position).toBe 12

  it 'should return position by id', ->
    expect(service.getPosition(1)).toBe 5
    expect(service.getPosition(9)).toBe 1
    expect(service.getPosition(8)).toBe 12

  it 'should return short name by id', ->
    expect(service.getShortName(1)).toBe 'ene'

  it 'should return month list', ->
    fixtures = FixturesLoader.enrollmentsCommitments()
    for fixture in fixtures
      check fixture.enrollment, fixture.commitment, fixture.size

  it 'should return initial month', ->
    expect(service.getInitialMonth().id).toEqual 8

  check = (enrollment, commitment, resultLength) ->
    result = service.getByEnrollmentAndCommitment(enrollment, commitment)
    expect(result.length).toBe resultLength

