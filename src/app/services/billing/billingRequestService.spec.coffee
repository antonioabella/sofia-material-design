'use strict'

describe 'BillingRequestService', ->
  service = {}
  resource = {}

  beforeEach ->
    module 'sofia'

    inject (BillingRequest) ->
      service = BillingRequest
      resource = jasmine.createSpyObj(['save'])
      service.resource = resource

  it 'should create and save billingRequest', ->
    conceptsToPay = []
    service.save(conceptsToPay)
    expect(resource.save).toHaveBeenCalled()
