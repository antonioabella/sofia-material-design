'use strict'

BillingConfigService = ($q, Month, Concept, ConceptCost) ->
  class BillingConfig
    load: (schoolYearId) ->
      $q.all
        months: Month.list()
        concepts: Concept.list()
        conceptCosts: ConceptCost.list(schoolYearId: schoolYearId)
      .then (config) -> config

  new BillingConfig()

BillingConfigService.$inject = ['$q', 'Month', 'Concept', 'ConceptCost']

angular.module 'sofia'
  .factory 'BillingConfig', BillingConfigService
