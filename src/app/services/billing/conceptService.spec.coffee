'use strict'

describe 'ConceptService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (Concept) ->
      service = Concept

  it 'should return toString', ->
    expect(service.toString()).toEqual('Concept')
