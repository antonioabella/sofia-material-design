'use strict'

describe 'ReceivableService', ->
  service = {}
  resource = {}

  beforeEach ->
    module 'sofia'

    inject (Receivable) ->
      service = Receivable
      resource = jasmine.createSpyObj(['query'])
      service.resource = resource

  it 'should list items', ->
    params = 'irrelevant'
    service.resource.query.and.returnValue buildPromise()
    service.list(params)
    expect(resource.query).toHaveBeenCalledWith(params)
    expect(_.size service.getPayers().A).toBe 3
    expect(_.size service.getPayers().C).toBe 2

  buildPromise = ->
    data = [
      payerName: 'A'
    ,
      payerName: 'A'
    ,
      payerName: 'A'
    ,
      payerName: 'C'
    ,
      payerName: 'C'
    ]
    $promise: createPromise(data)
