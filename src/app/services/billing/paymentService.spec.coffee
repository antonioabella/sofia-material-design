'use strict'

describe 'PaymentService', ->
  service = {}
  resource = {}

  beforeEach ->
    module 'sofia'

    inject (Payment) ->
      service = Payment
      resource = jasmine.createSpyObj(['save'])
      service.resource = resource

  it 'should create and save payment', ->
    service.save('irrelevant', 'irrelevant')
    expect(resource.save).toHaveBeenCalled()
