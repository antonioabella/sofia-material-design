'use strict'

describe 'CostService', ->
  service = {}
  ConceptToPay = {}
  Cost = {}

  beforeEach ->
    module 'sofia'

    inject (_Cost_, _ConceptToPay_) ->
      calculators = ['MONTHLY', 'ENROLLMENT', 'APPOINTED']
      costs = FixturesLoader.costs()
      Cost = _Cost_
      service = new Cost(costs, calculators)
      ConceptToPay = _ConceptToPay_

  it 'should returns concepts to pay with amount value', ->
    check ConceptToPayMocks.load(0, ConceptToPay),
      ConceptToPayWithCostMocks.load(0, ConceptToPay)

  it 'should throw error with invalid calculator name', ->
    error = new Error('Invalid cost name')
    expect(-> new Cost(null, ['irrelevant'])).toThrow error

  check = (conceptsToPay, conceptsToPayWithCosts) ->
    calculated = service.calculate(conceptsToPay)
    expect(calculated).toEqual conceptsToPayWithCosts
