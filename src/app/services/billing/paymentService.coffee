'use strict'

PaymentService = (DataService, SOFIA_API) ->
  class Payment extends DataService
    save: (invoices, paymentItems, partial = false) ->
      payment =
        invoices: invoices
        paymentItems: paymentItems
        partial: partial

      @resource.save payment

  new Payment(url: SOFIA_API.PAYMENT_URI)

PaymentService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Payment', PaymentService
