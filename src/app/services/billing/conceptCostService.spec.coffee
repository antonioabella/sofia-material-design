'use strict'

describe 'ConceptCostService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (ConceptCost) ->
      service = ConceptCost

  it 'should return toString', ->
    expect(service.toString()).toEqual('ConceptCost')
