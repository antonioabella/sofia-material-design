'use strict'

describe 'HouseholdService', ->
  service = {}
  ConceptToPay = {}

  beforeEach ->
    module 'sofia'

    inject (Household, _ConceptToPay_) ->
      service = new Household(FixturesLoader.enrollments(1),
        FixturesLoader.concepts())
      ConceptToPay = _ConceptToPay_

  it 'returns concepts to pay filtered by Household', ->
    expect(service.filter(ConceptToPayMocks.load(1, ConceptToPay))).toEqual(
      ConceptToPayHouseholdMocks.load(0, ConceptToPay))
