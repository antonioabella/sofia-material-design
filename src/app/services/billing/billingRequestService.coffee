'use strict'

BillingRequestService = (DataService, SOFIA_API) ->
  _createPaidConcepts = (conceptsToPay) ->
    for ctp in conceptsToPay
      enrollment: id: ctp.enrollmentId
      concept: id: ctp.conceptId
      month: id: ctp.monthId
      amount: ctp.amount
      discount: ctp.discount

  class BillingRequest extends DataService
    save: (conceptsToPay) ->
      billingRequest = paidConcepts: _createPaidConcepts(conceptsToPay)
      @resource.save billingRequest

  new BillingRequest(url: SOFIA_API.BILLING_REQUEST_URI)

BillingRequestService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'BillingRequest', BillingRequestService
