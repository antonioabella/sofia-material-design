'use strict'

describe 'BillingConfigService', ->
  service = {}
  Month = jasmine.createSpyObj 'Month', ['list']
  Concept = jasmine.createSpyObj 'Concept', ['list']
  ConceptCost = jasmine.createSpyObj 'ConceptCost', ['list']

  beforeEach ->
    module 'sofia'

    module ($provide) ->
      $provide.value 'Month', Month
      $provide.value 'Concept', Concept
      $provide.value 'ConceptCost', ConceptCost
      return

    inject (BillingConfig) ->
      service = BillingConfig

  it 'should load months, concepts and concept costs', ->
    schoolYearId = 'irrelevant'
    service.load(schoolYearId)
    expect(Month.list).toHaveBeenCalled()
    expect(Concept.list).toHaveBeenCalled()
    expect(ConceptCost.list).toHaveBeenCalledWith(schoolYearId: schoolYearId)
