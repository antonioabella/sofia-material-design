'use strict'

describe 'DiscountService', ->
  service = {}
  ConceptToPay = {}
  months = []

  beforeEach ->
    module 'sofia'

    enrollments = FixturesLoader.enrollments(0)

    inject (Discount, _ConceptToPay_) ->
      ConceptToPay = _ConceptToPay_
      service = new Discount(enrollments)

  it 'should return concepts to pay with discounts', ->
    check ConceptToPayWithCostMocks.load(0, ConceptToPay),
      ConceptToPayWithDiscountMocks.load(0, ConceptToPay)

  check = (toPay, toPayWithDiscount) ->
    expect(service.calculate(toPay)).toEqual toPayWithDiscount

