'use strict'

ConceptCostService = ($cacheFactory, DataService, SOFIA_API) ->
  _actions = query: {method: 'GET', cache: $cacheFactory, isArray: true}

  class ConceptCost extends DataService
    toString: -> 'ConceptCost'

  new ConceptCost(url: SOFIA_API.CONCEPT_COST_URI, actions: _actions)

ConceptCostService.$inject = ['$cacheFactory', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'ConceptCost', ConceptCostService
