'use strict'

_filteredCosts = (costs, name) ->
  _.filter costs, (cost) -> cost?.conceptCostType?.name is name

_conceptMatch = (i, c) -> i.conceptId is c.conceptId

_enrollmentCriteria = (i, c) ->
  _conceptMatch(i, c) and i.monthPosition is 0

_appointedCriteria = (i, c) ->
  _conceptMatch(i, c) and i.monthId is c.monthId

_monthlyCriteria = (i, c) ->
  _conceptMatch(i, c) and i.monthPosition >= c.monthPosition

_builder = (criteria, name) ->
  (concepts, costs) -> _.each _filteredCosts(costs, name), (cost) ->
    _.each concepts, (conceptToPay) ->
      if criteria(conceptToPay, cost)
        conceptToPay.amount = cost.amount

_buildCalculator = (name) ->
  switch name
    when 'MONTHLY' then _builder(_monthlyCriteria, name)
    when 'ENROLLMENT' then _builder(_enrollmentCriteria, name)
    when 'APPOINTED' then _builder(_appointedCriteria, name)
    else throw new Error 'Invalid cost name'

CostService = ->
  _calculators = []
  _costs = []

  class Cost
    constructor: (costs, calculators) ->
      _costs = costs
      _calculators = _.map calculators, (calculator) ->
        _buildCalculator calculator

    calculate: (conceptsToPay) ->
      for calculate in _calculators
        calculate conceptsToPay, _costs
      _.filter conceptsToPay, (concept) ->
        concept.amount? and concept.amount isnt 0

  Cost

angular.module 'sofia'
  .factory 'Cost', CostService
