'use strict'

ReceivableService = (DataService, SOFIA_API) ->
  _payers = {}

  class Receivable extends DataService
    getPayers: -> _payers

    list: (params) ->
      @resource.query(params).$promise.then (data) =>
        _payers = _.groupBy data, (invoice) -> invoice.payerName
        @items = data

  new Receivable(url: SOFIA_API.RECEIVABLE_URI)

ReceivableService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Receivable', ReceivableService
