'use strict'

BillingMonthService = ->
  ENROLLMENT_MONTH_SYSTEM_ID = 12
  INITIAL_MONTH_SYSTEM_ID = 7
  FINAL_MONTH_SYSTEM_ID = 6
  GET_ERROR_MSG = 'id must be number'

  _months = []
  _lastMonth = {}
  _initialMonth = {}

  _get = (id) ->
    monthId = _.parseInt(id)
    _.find _months, (m) -> m.id is monthId

  _getBySystemId = (systemId) ->
    _.find _months, (m) -> m.systemId is systemId

  _filterMonths = (subscription, months = _months) ->
    startMonth = _get(subscription.enrollmentMonth?.id)
    endMonth = _get(subscription.withdrawalMonth?.id)
    return [] unless startMonth
    _filterByStartAndEndPosition(months, startMonth, endMonth)

  _filterByStartAndEndPosition = (months, start, end = _lastMonth) ->
    months.filter (month) -> month.position >= start.position and
      month.position <= end.position

  _addEnrollmentMonth = (months) ->
    [_getBySystemId(ENROLLMENT_MONTH_SYSTEM_ID)].concat months

  _addInitialMonth = (months) ->
    [_initialMonth].concat months

  _addExtraMonths = (months) ->
    _addEnrollmentMonth(_addInitialMonth(months))

  _sort = (elements) -> _.sortBy elements, (o) -> o.position

  class BillingMonth
    constructor: (months) ->
      _months = months
      _lastMonth = _getBySystemId(FINAL_MONTH_SYSTEM_ID)
      _initialMonth = _getBySystemId(INITIAL_MONTH_SYSTEM_ID)

    getByEnrollmentAndCommitment: (enrollment, commitment) ->
      return [] unless enrollment.id is commitment.enrollmentId
      enrollmentMonths = _filterMonths(enrollment)
      _sort(_addExtraMonths _filterMonths(commitment, enrollmentMonths))

    getPosition: (monthId) -> _get(monthId).position

    getShortName: (monthId) -> _get(monthId).name.slice(0,3)

    getInitialMonth: -> _initialMonth

    get: _get

  BillingMonth

angular.module 'sofia'
  .factory 'BillingMonth', BillingMonthService
