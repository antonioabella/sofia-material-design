'use strict'

DiscountService = ->
  _validate = (conceptsToPay, discount) ->
    discount.active and conceptsToPay.conceptId is discount.conceptId and
      conceptsToPay.enrollmentId is discount.enrollmentId

  _applyDiscount = (conceptsToPay, discount) ->
    return unless _validate(conceptsToPay, discount)
    discount = _calculateDiscount(conceptsToPay, discount)
    conceptsToPay.discount = discount
    conceptsToPay.amount -= conceptsToPay.discount

  _calculateDiscount = (conceptToPay, discount) ->
    return discount.amount unless discount.isPercentage
    conceptToPay.amount * (discount.amount / 100)

  class Discount

    _discounts = []

    constructor: (balance) -> _discounts = balance.discounts

    calculate: (conceptsToPay) ->
      _.each _discounts, (discount) ->
        for concept in conceptsToPay
          _applyDiscount concept, discount
      conceptsToPay

  Discount

angular.module 'sofia'
  .factory 'Discount', DiscountService
