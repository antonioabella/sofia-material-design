'use strict'

ConceptToPayCalculatorService = (ConceptToPay) ->
  _balance = []
  _months = []

  _processBalance = ->
    _.flatten(_.map _balance.enrollments, (e) -> _processCommitments(e))

  _processCommitments = (enrollment) ->
    _.map _balance.commitments, (commitment) ->
      _createConcepts enrollment, commitment

  _createConcepts = (enrollment, commitment) ->
    _.map _months.getByEnrollmentAndCommitment(enrollment, commitment), (m) ->
      _createToPay enrollment, commitment, m unless _isPaid commitment, m

  _isPaid = (commitment, month) ->
    _.some _balance.paidConcepts, (paidConcept) ->
      paidConcept.enrollmentId is commitment.enrollmentId and
        paidConcept.conceptId is commitment.conceptId and
        paidConcept.monthId is month.id

  _createToPay = (enrollment, commitment, m) ->
    new ConceptToPay(enrollment.id, commitment.conceptId, m.id, m.position)

  class ConceptToPayCalculator
    constructor: (balance, months) ->
      _balance = balance
      _months = months

    build: ->
      _.compact(_.flatten(_processBalance()))

  ConceptToPayCalculator

ConceptToPayCalculatorService.$inject = ['ConceptToPay']

angular.module 'sofia'
  .factory 'ConceptToPayCalculator', ConceptToPayCalculatorService
