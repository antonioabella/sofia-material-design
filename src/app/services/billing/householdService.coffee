'use strict'

HouseholdService = ->
  _paidConcepts = []
  _hhConcepts = []
  _conceptsToPay = []

  _getHouseholdConcepts = ->
    _.filter _conceptsToPay, (ctp) ->
      _.any _hhConcepts, (concept) -> ctp.conceptId is concept.id

  _getUniqueHouseholdConcepts = (householdConcepts) ->
    _.uniq householdConcepts, (toPay) -> "#{toPay.conceptId}#{toPay.monthId}"

  _getUnpaidHouseholdConcepts = (householdConcepts) ->
    _.reject householdConcepts, (toPay) ->
      _.any _paidConcepts, (paid) ->
        toPay.conceptId is paid.conceptId and toPay.monthId is paid.monthId

  _conceptsToExclude = ->
    toPay = _getHouseholdConcepts()
    unpaid = _getUnpaidHouseholdConcepts(_getUniqueHouseholdConcepts(toPay))
    _.reject toPay, (ctp) -> _.any unpaid, (u) -> u is ctp

  class Household
    constructor: (balance, concepts) ->
      _paidConcepts = balance.paidConcepts
      _hhConcepts = _.filter concepts, (c) -> c.household

    filter: (conceptsToPay) ->
      _conceptsToPay = conceptsToPay
      toExclude = _conceptsToExclude()
      _.reject _conceptsToPay, (ctp) -> _.any toExclude, (e) -> e is ctp

  Household

angular.module 'sofia'
  .factory 'Household', HouseholdService
