'use strict'

describe 'ConceptToPayCalculatorService', ->
  ConceptToPayCalculator = {}
  ConceptToPay = {}
  months = {}

  beforeEach ->
    module 'sofia'

    inject (BillingMonth, _ConceptToPay_, _ConceptToPayCalculator_) ->
      ConceptToPayCalculator = _ConceptToPayCalculator_
      ConceptToPay = _ConceptToPay_
      months = new BillingMonth(FixturesLoader.months())

  it 'should returns concepts to pay from balance', ->
    for i in [0..1]
      check FixturesLoader.enrollments(i),
        ConceptToPayMocks.load(i, ConceptToPay)

  check = (balance, conceptToPay) ->
    service = new ConceptToPayCalculator(balance, months)
    expect(service.build()).toEqual conceptToPay
