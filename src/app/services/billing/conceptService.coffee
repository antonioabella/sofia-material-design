'use strict'

ConceptService = ($cacheFactory, DataService, SOFIA_API) ->
  _actions = query: {method: 'GET', cache: $cacheFactory, isArray: true}

  class Concept extends DataService
    toString: -> 'Concept'

  new Concept(url: SOFIA_API.CONCEPT_URI, actions: _actions)

ConceptService.$inject = ['$cacheFactory', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Concept', ConceptService
