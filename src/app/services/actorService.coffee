'use strict'

ActorService = (DataService, SOFIA_API) ->
  IMG_SVG = 'images/svg'
  _actions = update: method: 'PUT'
  _params = id: '@id', actorType: '@class'

  _setBirthDate = (actor) ->
    actor.person.birthDate = moment(actor.person.birthDate).toDate()
    actor

  _getIcon = (actorClass) ->
    switch actorClass
      when 'sofia.Student' then 'human-child.svg'
      when 'sofia.Guardian' then 'human-male-female.svg'
      else 'school.svg'

  class Actor extends DataService
    SEARCH_ACTIVE: 'ACTIVE'
    SEARCH_INACTIVE: 'INACTIVE'
    SEARCH_ALL: 'ALL'

    getIcon: (actor = @item) -> "#{IMG_SVG}/#{_getIcon(actor?.class)}"
    getIconStyle: (actor = @item) -> actor?.class.replace('.', '-')
    isStudent: (actor = @item) -> actor?.class == 'sofia.Student'
    isGuardian: (actor = @item) -> actor?.class == 'sofia.Guardian'
    isTeacher: (actor = @item) -> actor?.class == 'sofia.Teacher'

    get: (params) -> super(params).then _setBirthDate
    save: (item) -> super(item).then _setBirthDate

  new Actor(url: SOFIA_API.ACTOR_URI, params: _params, actions: _actions)

ActorService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Actor', ActorService
