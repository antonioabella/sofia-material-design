'use strict'

describe 'DataService', ->
  LOCALE_URI = '$locale$/irrelevant/irrelevant/:id'
  URI = 'app/i18n/es-ve/irrelevant/irrelevant/:id'
  $httpBackend = {}
  id = 55
  item = id: id
  service = {}

  beforeEach ->
    module 'sofia'

    inject ($resource, _$httpBackend_, DataService) ->
      $httpBackend = _$httpBackend_
      service = createService(DataService)

  checkList = (code, items) ->
    $httpBackend.expect('GET', ParamsHelper.getUrl(URI)).respond code, items
    service.list()
    $httpBackend.flush()
    expect(_.isEqual service.items, items).toBeTruthy()

  it 'should list items', ->
    checkList 400, []
    checkList 200, ['irrelevant']

  it 'should get item', inject ($resource) ->
    $httpBackend.expect('GET', ParamsHelper.getUrl(URI)).respond(200, item)
    service.get()
    $httpBackend.flush()
    expect(_.isEqual service.item.id, item.id).toBeTruthy()

  it 'should select item by id', ->
    service.items = [{id: 2}, {id: 5}, item]
    service.select id
    expect(service.item).toEqual item

  it 'should create new item', ->
    expect(service.item).toEqual {}
    service.create()
    expect(service.item).not.toEqual {}
    expect(service.item.$save).toBeDefined()

  it 'should set item to empty object', ->
    service.create()
    expect(service.item).not.toEqual {}
    service.unselect()
    expect(service.item).toEqual {}

  it 'should update items array', ->
    service.items = [{id: '1', name: 'oldValue'}]
    service._updateItems({id: '1', name: 'newValue'})
    expect(service.items[0].name).toBe 'newValue'
    service._updateItems({id: '2', name: 'newValue'})
    expect(service.items.length).toBe 2

  it 'should delete item', ->
    id = '1'
    item = createItem(id)
    service.items = [id: id]
    service.delete item
    expect(item.$delete).toHaveBeenCalled()
    expect(service.items.length).toBe 0

  it 'should copy item', ->
    id = 44
    original = service.create()
    original.id = id
    copycat = service.copy()
    expect(original.id).toEqual(copycat.id)

  it 'should save new item', ->
    item = createItem()
    service.save item
    expect(item.$save).toHaveBeenCalled()

  it 'should update item', ->
    item = createItem('irrelevant')
    service.save item
    expect(item.$update).toHaveBeenCalled()

  it 'should call itemsUpdate function', ->
    service.itemsUpdated = jasmine.createSpy 'itemsUpdated'
    service.save(createItem())
    expect(service.itemsUpdated).toHaveBeenCalled()

  createItem = (id) ->
    item = jasmine.createSpyObj ['$save', '$update', '$delete']
    item.id = id
    item.$save.and.returnValue createPromise(item)
    item.$update.and.returnValue createPromise(item)
    item.$delete.and.returnValue createPromise(item)
    item

  createService = (DataService) ->
    params = id: '@id'
    actions = update: method: 'PUT'
    class Test extends DataService

    new Test(url: LOCALE_URI, params: params, actions: actions)
