'use strict'

StudentEnrollmentsService = (DataService, SOFIA_API) ->
  params = studentId: '@studentId', id: '@id'
  actions = update: method: 'PUT'

  _extraData = (enrollment) ->
    enrollment.gradeName = enrollment.grade.name
    enrollment.sectionName = enrollment.section.name
    enrollment.start = enrollment.schoolYear.start
    enrollment.end = enrollment.schoolYear.end
    enrollment

  class StudentEnrollments extends DataService
    get: (params) -> super(params).then _extraData
    save: (item) -> super(item).then _extraData

  new StudentEnrollments(
    url: SOFIA_API.STUDENT_ENROLLMENTS_URI, params: params, actions: actions)

StudentEnrollmentsService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'StudentEnrollments', StudentEnrollmentsService
