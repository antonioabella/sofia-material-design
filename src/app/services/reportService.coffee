'use strict'

ReportService = (DataService, SOFIA_API) ->
  class Report extends DataService

  new Report(url: SOFIA_API.REPORT_URI, params: id: '@id')

ReportService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Report', ReportService
