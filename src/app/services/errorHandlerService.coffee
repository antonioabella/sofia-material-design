'use strict'

ErrorHandlerService = ($mdToast) ->
  class ErrorHandler
    GENERIC_ERROR = 'Ha ocurrido un error. No se pudo completar la operación'
    showErrors: (error) ->
      console.error error
      msgs = _.pluck error.data.errors, 'message'
      msg = if _.isEmpty msgs then GENERIC_ERROR else msgs.join('\n')
      @showMessage msg

    showMessage: (msg) ->
      $mdToast.show($mdToast.simple().content(msg))

  new ErrorHandler()

ErrorHandlerService.$inject = ['$mdToast']

angular.module 'sofia'
  .factory 'ErrorHandler', ErrorHandlerService
