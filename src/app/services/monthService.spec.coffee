'use strict'

describe 'MonthService', ->
  service = {}
  months = [
    {id: 1, systemId: 0}
    {id: 2, systemId: 1}
    {id: 3, systemId: 2}
  ]

  beforeEach ->
    module 'sofia'

    inject (_Month_) ->
      service = _Month_

  it 'should get default month', ->
    today = new Date('2014-03-07')
    CURRENT_MONTH_IDX = 2
    service.items = months
    expect(service.getDefault()).not.toBeNull()
    expect(service.getDefault(today)).toEqual months[CURRENT_MONTH_IDX]

  it 'should get items without enrollment', ->
    months.push {id: 4, systemId: service.ENROLLMENT_MONTH_SID}
    service.items = months
    expect(_.size service.getItemsWithoutEnrollment()).toBe (_.size months) - 1

  it 'should return toString', ->
    expect(service.toString()).toEqual('Month')
