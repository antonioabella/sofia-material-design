'use strict'

ActorGuardianService = (DataService, SOFIA_API) ->
  class ActorGuardian extends DataService

  new ActorGuardian(url: SOFIA_API.ACTOR_GUARDIANS_URI)

ActorGuardianService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'ActorGuardian', ActorGuardianService
