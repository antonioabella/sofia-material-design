'use strict'

describe 'CountryService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (Country) ->
      service = Country

  it 'should returns the default country', ->
    countries = [{id: 'US'}, {id: 'VE'}]
    expect(service.getDefault(countries)?.id).toBe countries[1].id
