'use strict'

CustodyService = (DataService, SOFIA_API) ->
  _getCollection = (actor) ->
    if actor.class == 'sofia.Student'
      actor['custodians']
    else
      actor['custodianships']

  _getType = (actor) ->
    if actor.class == 'sofia.Student' then 'guardian' else 'student'

  _getChildrenClass = (actor) ->
    if actor.class == 'sofia.Student' then 'sofia.Guardian' else 'sofia.Student'

  _beforeSave = (c) ->
    c.student = id: c.student.id
    c.guardian = id: c.guardian.id
    c

  _enhanceCustody = (actor) ->
    type = _getType(actor)
    (c) ->
      c.actorId = c[type].id
      c.person = c[type].person
      c

  class Custody extends DataService
    getCustodies: (actor) ->
      type = _getType(actor)
      @items = _.map _getCollection(actor), _enhanceCustody(actor)
      @items

    save: (item, actor) ->
      super(_beforeSave(item)).then _enhanceCustody(actor)

    getChildrenClass: _getChildrenClass

  new Custody(url: SOFIA_API.CUSTODY_URI)

CustodyService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Custody', CustodyService
