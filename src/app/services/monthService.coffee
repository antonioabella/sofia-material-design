'use strict'

MonthService = ($cacheFactory, DataService, SOFIA_API) ->
  _ENROLLMENT_MONTH_SID = 12
  _actions = query: {method: 'GET', cache: $cacheFactory, isArray: true}

  class Month extends DataService
    toString: -> 'Month'

    getDefault: (today = new Date()) ->
      _.find @items, (m) -> m.systemId is today.getMonth()

    getItemsWithoutEnrollment: ->
      _.filter @items, (i) -> i.systemId isnt _ENROLLMENT_MONTH_SID

    ENROLLMENT_MONTH_SID: _ENROLLMENT_MONTH_SID

  new Month(url: SOFIA_API.MONTH_URI, actions: _actions)

MonthService.$inject = ['$cacheFactory', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Month', MonthService
