'use strict'

describe 'DebtBalanceService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (DebtBalance) ->
      service = DebtBalance

  it 'should return toString', ->
    expect(service.toString()).toEqual('DebtBalance')
