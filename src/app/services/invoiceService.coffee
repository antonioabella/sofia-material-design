'use strict'

InvoiceService = (DataService, SOFIA_API) ->
  class Invoice extends DataService

  new Invoice(url: SOFIA_API.INVOICE_URI, params: id: '@id')

InvoiceService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Invoice', InvoiceService
