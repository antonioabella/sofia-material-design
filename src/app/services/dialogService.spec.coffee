'use strict'

describe 'DialogService', ->
  service = {}
  $mdDialog = {}
  ErrorHandler = {}
  error = 'irrelevant'
  dialog =
    ds: jasmine.createSpyObj 'Data', ['save', 'delete']

  beforeEach ->
    module 'sofia'

    inject (_$mdDialog_, _ErrorHandler_, Dialog) ->
      $mdDialog = _$mdDialog_
      spyOn $mdDialog, 'show'
        .and.returnValue createPromise()
      service = Dialog.create(dialog)
      ErrorHandler = _ErrorHandler_
      spyOn ErrorHandler, 'showErrors'

      expect(service.cancel).toEqual($mdDialog.cancel)

  it 'should show dialog', ->
    service.showDialog()
    expect($mdDialog.show).toHaveBeenCalled()

  it 'should open menu', ->
    $event = 'irrelevant'
    $mdOpenMenu = jasmine.createSpy()
    service.openMenu($mdOpenMenu, $event)
    expect($mdOpenMenu).toHaveBeenCalledWith($event)

  describe 'should save item', ->
    it 'and close dialog', ->
      dummy = 'irrelevant'
      spyOn $mdDialog, 'cancel'
      dialog.ds.save.and.returnValue createPromise()
      service.save(dummy)
      expect(dialog.ds.save).toHaveBeenCalledWith(dummy)
      expect($mdDialog.cancel).toHaveBeenCalled()

    it 'or show errors', inject (ErrorHandler) ->
      checkError 'save', service.save

  describe 'should delete item', ->
    it 'and show confirm dialog', ->
      dummy = 'irrelevant'
      dialog.ds.delete.and.returnValue createPromise()
      spyOn $mdDialog, 'confirm'
        .and.callThrough()
      service.deleteItem(dummy)
      expect(dialog.ds.delete).toHaveBeenCalledWith(dummy)

    it 'or show errors', inject (ErrorHandler) ->
      checkError 'delete', service.deleteItem

  checkError = (methodName, fun) ->
    dialog.ds[methodName].and.returnValue createRejectPromise(error)
    fun()
    expect(ErrorHandler.showErrors).toHaveBeenCalledWith(error)
