'use strict'

DebtBalanceService = (DataService, SOFIA_API) ->
  _actions = 'query': {method: 'GET', isArray: false}

  class DebtBalance extends DataService
    toString: -> 'DebtBalance'

  new DebtBalance(url: SOFIA_API.DEBT_BALANCE_URI, actions: _actions)

DebtBalanceService.$inject = ['DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'DebtBalance', DebtBalanceService
