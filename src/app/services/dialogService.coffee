'use strict'

DialogService = ($mdDialog, ErrorHandler) ->
  Service = (dialog) ->
    _createConfirm = ->
      $mdDialog.confirm()
        .parent(angular.element(document.body))
        .title '¿Desea eliminar?'
        .textContent 'La información eliminada no podrá restituirse'
        .ariaLabel 'Eliminar'
        .ok 'Si, deseo eliminar la información'
        .cancel 'Cancelar elimnación'
    _showErrors = (error) -> ErrorHandler.showErrors error

    service =
      showDialog: ->
        $mdDialog.show
          parent: angular.element(document.body)
          controllerAs: 'vm'
          controller: dialog.controller
          templateUrl: dialog.templateUrl
          locals: dialog.locals

      cancel: $mdDialog.cancel

      save: (origin) ->
        dialog.ds.save(origin).then (item) ->
          $mdDialog.hide(item)
        , _showErrors

      openMenu: ($mdOpenMenu, $event) -> $mdOpenMenu($event)

      deleteItem: (item) ->
        $mdDialog.show(_createConfirm()).then ->
          dialog.ds.delete(item).then ->
            return
          , _showErrors


  create: (dialog) -> new Service(dialog)

DialogService.$inject = ['$mdDialog', 'ErrorHandler']

angular.module 'sofia'
  .factory 'Dialog', DialogService
