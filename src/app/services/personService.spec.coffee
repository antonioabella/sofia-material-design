'use strict'

describe 'PersonService', ->
  service = {}
  API = {}
  $httpBackend = {}

  beforeEach ->
    module 'sofia'

    inject (_$httpBackend_, Person, SOFIA_API) ->
      API = SOFIA_API
      service = Person
      $httpBackend = _$httpBackend_

  it 'should return person image', ->
    person = picture: 'p'
    expect(service.getImage(person)).toEqual('images/people/p')

  it 'should return identity card', ->
    person =
      identityCard: '123456789'
      outsider: true
    expect(service.getIdentityCard(person)).toEqual 'E-123.456.789'
    person.outsider = false
    service.item = person
    expect(service.getIdentityCard(person)).toEqual 'V-123.456.789'

  it 'should return gender', ->
    person = gender: 'MALE'
    expect(service.getGender(person)).toEqual 'Varón'
    person = gender: 'FEMALE'
    expect(service.getGender(person)).toEqual 'Hembra'

  it 'should return age', ->
    person = birthDate: '2010-08-31'
    date = '2014-08-31'
    expect(service.getAge(person, date)).toBe '4 años'

  it 'should return full name', ->
    person = firstName: 'A', lastName: 'B'
    expect(service.getFullName(person)).toEqual 'B, A'

  it 'should return gender icon name', ->
    person = gender: 'MALE'
    expect(service.getGenderIconName(person)).toEqual 'gender-male'
    person = gender: 'FEMALE'
    expect(service.getGenderIconName(person)).toEqual 'gender-female'

  checkDate = (httpVerb, f) ->
    person = createPerson()
    params = ParamsHelper.getUrl(API.PERSON_URI)
    $httpBackend.expect(httpVerb, params).respond 200, person
    f(person)
    $httpBackend.flush()
    expect(_.isDate service.item.birthDate).toBeTruthy()

  createPerson = ->
    person = jasmine.createSpyObj('person', ['$save'])
    person.$save.and.returnValue createPromise(person)
    person.birthDate = '2007-01-01'
    person

