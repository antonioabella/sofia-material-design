'use strict'

DataService = ($resource, $locale) ->
  _itemFinder = (id) -> (e) -> e.id is id

  _getUrl = (uri) ->
    return uri unless _.contains(uri, '$locale$')
    "app/i18n/#{uri.replace('$locale$', $locale.id)}"

  class Data
    constructor: (config) ->
      url = _getUrl config.url
      @resource = new $resource(url, config.params, config.actions)

    _findItem: (id) => _.find @items, _itemFinder(id)

    _notify: -> @itemsUpdated() if @itemsUpdated?

    _updateItems: (item) =>
      oldItem = @_findItem(item.id)
      angular.copy(item, oldItem) if oldItem?
      @items?.push(item) unless oldItem?
      @_notify()
      @item = item
      item

    _removeItem: (item) =>
      _.remove @items, _itemFinder(item.id)
      @_notify()

    items: []

    item: {}

    findItem: (id) -> @_findItem(id)

    list: (params) ->
      @items = []
      @resource.query(params).$promise.then (data) => @items = data

    get: (params) -> @resource.get(params).$promise.then (data) =>
      @item = data

    create: -> @item = new @resource()

    unselect: -> @item = {}

    select: (id) -> @item = angular.copy(@_findItem id)

    copy: -> angular.copy(@item)

    save: (item) ->
      if item.id?
        item.$update().then @_updateItems
      else
        item.$save().then @_updateItems

    delete: (item) -> item.$delete().then @_removeItem

DataService.$inject = ['$resource', '$locale']

angular.module 'sofia'
  .factory 'DataService', DataService

