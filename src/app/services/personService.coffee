'use strict'

PersonService = ($filter, $translate, DataService, SOFIA_API) ->
  IMG_SVG = 'images/svg'
  IMG_PPL = 'images/people'

  _actions = update: method: 'PUT'
  _params = id: '@id'

  class Person extends DataService
    getImage: (person) -> "#{IMG_PPL}/#{person?.picture ? 'default.png'}"

    getGender: (person) -> $translate.instant(person?.gender)

    getAge: (person, date = moment.today) ->
      age = moment(date).diff(moment(person?.birthDate), 'years')
      "#{age} #{$translate.instant('YEARS')}"

    getIdentityCard: (person) ->
      prefix = if person?.outsider then 'E-' else 'V-'
      identityCard = $filter('number')(person?.identityCard)
      "#{prefix}#{identityCard}"

    getFullName: (person) -> "#{person?.lastName}, #{person?.firstName}"

    getGenderIconName: (person) -> "gender-#{person?.gender.toLowerCase()}"


  new Person(url: SOFIA_API.PERSON_URI, params: _params, actions: _actions)

PersonService.$inject = ['$filter', '$translate', 'DataService', 'SOFIA_API']

angular.module 'sofia'
  .factory 'Person', PersonService
