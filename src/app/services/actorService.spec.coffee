'use strict'

describe 'ActorService', ->
  service = {}

  beforeEach ->
    module 'sofia'

    inject (Actor) ->
      service = Actor

  it 'should return actor icon', ->
    actors = [
      icon: 'images/svg/human-child.svg', iconStyle: 'sofia-Student'
      class: 'sofia.Student'
    ,
      icon: 'images/svg/human-male-female.svg', iconStyle: 'sofia-Guardian'
      class: 'sofia.Guardian'
    ,
      icon: 'images/svg/school.svg', iconStyle: 'sofia-Teacher'
      class: 'sofia.Teacher'
    ].forEach (actor) -> checkIcon actor.class, actor.icon, actor.iconStyle

  it 'should identify actor class', ->
    actor = class: 'sofia.Student'
    expect(service.isStudent(actor)).toBeTruthy()
    actor.class = 'sofia.Guardian'
    expect(service.isGuardian(actor)).toBeTruthy()
    actor.class = 'sofia.Teacher'
    expect(service.isTeacher(actor)).toBeTruthy()

  checkIcon = (actorClass, icon, iconStyle) ->
    actor = class: actorClass
    expect(service.getIcon(actor)).toEqual(icon)
    expect(service.getIconStyle(actor)).toEqual(iconStyle)
