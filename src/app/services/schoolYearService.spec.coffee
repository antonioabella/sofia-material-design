'use strict'

describe 'SchoolYearService', ->
  service = {}
  years = [
    {id: 1, active: false, enrollment: true}
    {id: 2, active: true, enrollment: false}
    {id: 3, active: false, enrollment: false}
  ]

  beforeEach ->
    module 'sofia'

    inject (SchoolYear) ->
      service = SchoolYear

  it 'should get default school year (active)', ->
    ACTIVE_YEAR = 1
    service.items = years
    expect(service.getDefault()).toEqual years[ACTIVE_YEAR]
    expect(service.getActive()).toEqual years[ACTIVE_YEAR]

  it 'should get enrollment schoolYear', ->
    ENROLLMENT_YEAR = 0
    service.items = years
    expect(service.getEnrollment()).toEqual years[ENROLLMENT_YEAR]

  it 'should returns schoolYear name', ->
    schoolYear = {start: new Date('2013-07-01'), end: new Date('2014-04-01')}
    expect(service.getName(schoolYear)).toBe '2013 - 2014'
    schoolYear = {id: 0, name: 'Show All'}
    expect(service.getName(schoolYear)).toBe 'Show All'

  it 'should return toString', ->
    expect(service.toString()).toEqual('SchoolYear')
