'use strict'

routerConfig = ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state 'main',
      url: '/main'
      templateUrl: 'components/main/main.html'
    .state 'sandbox',
      url: '/sandbox'
      templateUrl: 'components/sandbox/sandbox.html'
      controller: 'SandboxController'
      controllerAs: 'vm'

  $urlRouterProvider.otherwise '/main'

dateLocaleConfig = ($mdDateLocaleProvider) ->
  $mdDateLocaleProvider.parseDate = (dateString) ->
    m = moment(dateString, 'DD/MM/YYYY', true)
    if m.isValid() then m.toDate() else new Date(NaN)

  $mdDateLocaleProvider.formatDate = (date) ->
    moment(date).format('DD/MM/YYYY')

angular.module 'sofia'
  .config ['$stateProvider', '$urlRouterProvider', routerConfig]

  .config ['$mdThemingProvider', ($mdThemingProvider) ->
    $mdThemingProvider.theme 'default'
      .primaryPalette 'light-blue'
      .accentPalette 'pink'
  ]

  .config ['$translateProvider', ($translateProvider) ->
    $translateProvider.preferredLanguage('es_VE')
    $translateProvider.useSanitizeValueStrategy('escape')
  ]

  .config ['$mdDateLocaleProvider', dateLocaleConfig]
