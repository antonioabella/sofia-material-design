'use strict'

angular.module 'sofia.core', [
  'ngResource'
  'ui.router'
  'ngSanitize'
  'ngAnimate'
  'ngMaterial'

  'pascalprecht.translate'
  'ui.grid'
  'ui.grid.pinning'
  'ui.grid.exporter'
  'ui.grid.edit'
  'ui.grid.rowEdit'
  'ui.grid.cellNav'
  'ui.grid.autoResize'
  'cgBusy'
  'angular-loading-bar'
]
