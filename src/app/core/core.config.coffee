'use strict'

angular.module 'sofia.core'

.config ['$compileProvider'
  ($compileProvider) ->
    pattern = /^\s*(https?|ftp|mailto|file|javascript):/
    $compileProvider.aHrefSanitizationWhitelist pattern
]

.config ['$translateProvider'
  ($translateProvider) -> $translateProvider.preferredLanguage('es_VE')]


