'use strict'

translations =
  MALE: 'Varón'
  FEMALE: 'Hembra'
  YEARS: 'años'

  GENERAL:
    EDIT_LABEL: 'Editar'
    SAVE_LABEL: 'Guardar'
    CANCEL_LABEL: 'Cancelar'
    CREATE_LABEL: 'Crear'
    SEARCH_LABEL: 'Buscar'

    SECTION_LABEL: 'Sección'
    BANK_LABEL: 'Banco'
    SCHOOL_YEAR_LABEL: 'Periodo Escolar'
    PAYMENT_METHOD_LABEL: 'Forma de Pago'
    KINSHIP_LABEL: 'Parentesco'
    MONTH_LABEL: 'Mes'
    GRADE_LABEL: 'Grado'
    CONCEPT_LABEL: 'Concepto'

angular.module 'sofia'
.config ['$translateProvider', ($translateProvider) ->
  $translateProvider.translations 'es_VE', translations
]
