'use strict'

angular.module 'sofia.core'
.constant 'SOFIA_ERRORS', {
  UNDEFINED_EVALUATION: 'modalForm.$scope must have an evaluation'
}
