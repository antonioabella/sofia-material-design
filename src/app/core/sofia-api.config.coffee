'use strict'

baseUrl = 'http://192.168.1.175:8080'

API_VERSION = "#{baseUrl}/api"

angular.module 'sofia.core'

.constant 'SOFIA_API', {
  ACTOR_EMAILS_URI: 'actors/:actorId/emails/:id'
  ACTOR_PHONES_URI: 'actors/:actorId/phones/:id'
  ACTOR_GUARDIANS_URI: "#{API_VERSION}/actors/:actorId/guardians"
  ACTOR_URI: "#{API_VERSION}/actors/:id"
  BANK_URI: "#{API_VERSION}/banks/:id"
  BILLING_GROUP_URI: 'billingGroups'
  BILLING_REQUEST_URI: "#{API_VERSION}/billingRequests"
  COUNTRY_URI: '$locale$/countries.json'
  CONCEPT_URI: "#{API_VERSION}/concepts"
  CONCEPT_COST_URI: "#{API_VERSION}/conceptCosts"
  COURSE_URI: 'courses'
  CUSTODY_URI: "#{API_VERSION}/custodies/:id"
  DEBT_BALANCE_URI: "#{API_VERSION}/debtBalances"
  GENDER_URI: '$locale$/genders.json'
  GRADE_URI: "#{API_VERSION}/grades"
  GRADE_ENROLLMENTS_URI: "#{API_VERSION}/grades/:gradeId/enrollments/:id"
  INVOICE_URI: "#{API_VERSION}/invoices/:id"
  INVOICE_PRINT_URI: "#{baseUrl}/invoice/print"
  EVALUATION_URI: 'evaluations/:id'
  KINSHIP_URI: '$locale$/kinships.json'
  MONTH_URI: "#{API_VERSION}/months"
  PAYMENT_URI: "#{API_VERSION}/payments"
  PAYMENT_METHOD_URI: "#{API_VERSION}/paymentMethods"
  PERIOD_URI: 'periods'
  PERIOD_WEEK_URI: 'periodWeeks'
  PERSON_URI: "#{API_VERSION}/people/:id"
  RECEIVABLE_URI: "#{API_VERSION}/receivables"
  REPORT_URI: "#{API_VERSION}/reports/:id"
  REPORT_PRINT_URI: "#{baseUrl}/report/print"
  RESET_URI: 'resetTestDatabase'
  SCHOOL_YEAR_URI: "#{API_VERSION}/schoolYears"
  SECTION_URI: "#{API_VERSION}/sections/:id"
  SECTION_ENROLLMENTS_URI: "#{API_VERSION}/sections/:sectionId/enrollments/:id"
  STUDENT_ENROLLMENTS_URI: "#{API_VERSION}/students/:studentId/enrollments/:id"
  ENROLLED_COURSE_URI: 'enrolledCourses/:id'
}

.constant 'ENROLLMENT_MONTH_SID', 12
#.constant 'BILLING_RESUME_RESOURCE', baseUrl + 'billingResume'
#.constant 'GUARDIAN_ENROLLMENT_RESOURCE', baseUrl +
#  'guardians/:guardianId/enrollments/:id'
#.constant 'GUARDIAN_RESOURCE', baseUrl + 'guardians/:id'
#.constant 'INVOICE_RESUME_RESOURCE', baseUrl + 'invoiceResume/:id'
#

