'use strict'

angular.module 'sofia', [
  'sofia.core'
  'sofia.components'

  'sofia.actor'
  'sofia.billing'
  'sofia.enrollment'
  'sofia.invoice'
  'sofia.report'
  'sofia.section'
]
