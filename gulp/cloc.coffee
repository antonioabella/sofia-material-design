'use strict'

gulp = require('gulp')

$ = require('gulp-load-plugins')()

module.exports = (options) ->
  gulp.task 'cloc:src', ->
    gulp.src [
      "#{options.src}/**/*.coffee"
      "!#{options.src}/**/*.spec.coffee"
    ]
      .pipe $.sloc()

  gulp.task 'cloc:test', ->
    gulp.src [
      "#{options.src}/**/*.spec.coffee"
      "#{options.test}/**/*.coffee"
    ]
      .pipe $.sloc()

  gulp.task 'cloc:all', ->
    gulp.src [
      "#{options.src}/**/*.coffee"
      "#{options.test}/**/*.coffee"
    ]
      .pipe $.sloc()

  gulp.task 'cloc', ['cloc:test', 'cloc:src', 'cloc:all'], (done) -> done()
