'use strict'

gulp = require('gulp')
browserSync = require('browser-sync')
cache = require('gulp-cached')

$ = require('gulp-load-plugins')()

module.exports = (options) ->
  markups = [
    "#{options.src}/#{options.app}/**/*.jade"
    "!#{options.src}/#{options.app}/**/_*.jade"
  ]

  gulp.task 'markups', ->
    gulp.src markups
      .pipe cache 'markups'
      .pipe $.jade basedir: options.src, doctype: 'html', pretty: true
      .on 'error', options.errorHandler 'Jade'
      .pipe gulp.dest "#{options.tmp}/serve/"
      .pipe browserSync.reload stream: true

