'use strict'

gulp = require('gulp')
browserSync = require('browser-sync')
browserSyncSpa = require('browser-sync-spa')

util = require('util')

module.exports = (options) ->
  browserSyncInit = (baseDir, browser) ->
    browser ?= 'default'

    routes = null
    if baseDir is options.src or (util.isArray(baseDir) and
        baseDir.indexOf(options.src) isnt -1)
      routes =
        '/bower_components': 'bower_components'
        '/node_modules': 'node_modules'

    server =
      baseDir: baseDir
      routes: routes

    browserSync.instance = browserSync.init
      startPath: '/'
      server: server
      browser: browser

  browserSync.use browserSyncSpa(selector: '[data-ng-app]')

  gulp.task 'serve', ['watch'], ->
    browserSyncInit ["#{options.tmp}/serve", options.src], []

  gulp.task 'serve:dist', ['build'], ->
    browserSyncInit options.dist, []

  gulp.task 'e2e', ['inject'], ->
    browserSyncInit ["#{options.tmp}/serve", options.src], []

  gulp.task 'serve:e2e-dist', ['build'], ->
    browserSyncInit options.dist, []

