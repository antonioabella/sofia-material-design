'use strict'

gulp = require('gulp')
browserSync = require('browser-sync')
cache = require('gulp-cached')

$ = require('gulp-load-plugins')()

module.exports = (options) ->
  scripts = [
    "#{options.src}/#{options.app}/**/*.coffee"
    "!#{options.src}/#{options.app}/**/*.spec.coffee"
  ]
  gulp.task 'scripts', ->
    gulp.src scripts
      .pipe cache 'scripts'
      .pipe $.sourcemaps.init()
      .pipe $.coffeelint()
      .pipe $.coffeelint.reporter()
      .pipe($.coffee()).on 'error', options.errorHandler 'CoffeeScript'
      .pipe $.sourcemaps.write()
      .pipe gulp.dest "#{options.tmp}/serve/"
      .pipe browserSync.reload stream: true
      .pipe $.size()
