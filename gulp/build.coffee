'use strict'

gulp = require('gulp')

$ = require('gulp-load-plugins')(
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
)

module.exports = (options) ->
  gulp.task 'partials', ['markups'], ->
    gulp.src [
      "#{options.src}/#{options.app}/**/*.html"
      "#{options.tmp}/serve/#{options.app}/**/*.html"
    ]
      .pipe $.minifyHtml
        empty: true
        spare: true
        quotes: true
      .pipe $.angularTemplatecache 'templateCacheHtml.js',
        module: 'sofia'
      .pipe gulp.dest "#{options.tmp}/partials/"

  gulp.task 'html', ['inject', 'partials'], ->
    partialsInjectFile = gulp.src(
      "#{options.tmp}/partials/templateCacheHtml.js", read: false)

    partialsInjectOptions =
      starttag: '<!-- inject:partials -->'
      ignorePath: "#{options.tmp}/partials"
      addRootSlash: false

    htmlFilter = $.filter '*.html', restore: true
    jsFilter = $.filter '**/*.js', restore: true
    cssFilter = $.filter '**/*.css', restore: true
    assets = ''

    gulp.src "#{options.tmp}/serve/*.html"
      .pipe $.inject(partialsInjectFile, partialsInjectOptions)
      .pipe(assets = $.useref.assets())
      .pipe $.rev()
      .pipe jsFilter
      .pipe $.ngAnnotate()
      .pipe $.uglify(preserveComments: $.uglifySaveLicense).on(
        'error', options.errorHandler('Uglify'))
      .pipe jsFilter.restore()
      .pipe cssFilter
      .pipe $.csso()
      .pipe cssFilter.restore()
      .pipe assets.restore()
      .pipe $.useref()
      .pipe $.revReplace()
      .pipe htmlFilter
      .pipe $.minifyHtml
        empty: true
        spare: true
        quotes: true
        conditionals: true
      .pipe htmlFilter.restore()
      .pipe gulp.dest("#{options.dist}/")
      .pipe $.size(title: "#{options.dist}/", showFiles: true)

  gulp.task 'fonts', ->
    gulp.src $.mainBowerFiles()
      .pipe $.filter ['**/*.{eot,ttf,svg,woff,woff2}', '!**/ui*']
      .pipe $.flatten()
      .pipe gulp.dest("#{options.dist}/fonts/")

  gulp.task 'images', ->
    gulp.src "#{options.src}/images/**/*"
      .pipe $.imagemin().on 'error', options.errorHandler('ImageMin')
      .pipe gulp.dest "#{options.dist}/images"

  gulp.task 'other', ->
    gulp.src [
      "#{options.src}/**/*"
      "!#{options.src}/images/**/*"
      "!#{options.src}/**/*.{html,css,js,less,coffee,jade}"
    ]
      .pipe gulp.dest "#{options.dist}/"

  gulp.task 'ui-grid', ->
    gulp.src $.mainBowerFiles()
      .pipe $.filter '**/ui*.{eot,ttf,svg,woff,woff2}'
      .pipe $.flatten()
      .pipe gulp.dest("#{options.dist}/styles/")

  gulp.task 'clean', (done) ->
    $.del ["#{options.dist}/", "#{options.tmp}/", 'dist.zip'], done

  gulp.task 'build', ['html', 'fonts', 'images', 'other', 'ui-grid']
