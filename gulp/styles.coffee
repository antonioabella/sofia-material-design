'use strict'

gulp = require('gulp')
browserSync = require('browser-sync')

$ = require('gulp-load-plugins')()

module.exports = (options) ->
  lessFiles = [
    "#{options.src}/#{options.app}/**/*.less"
    "!#{options.src}/app/index.less"
  ]

  gulp.task 'styles', ->
    lessOptions =
      options: [
        'bower_components'
        "#{options.src}/app"
        "#{options.src}/components"
      ]

    injectFiles = gulp.src lessFiles, read: false

    injectOptions =
      transform: (filePath) ->
        filePath = filePath.replace("#{options.src}/app/", '')
        filePath = filePath.replace("#{options.src}/components/",
          '../components/')
        filePath = filePath.replace("#{options.src}/directive/",
          '../directive/')
        "@import '#{filePath}';"
      starttag: '// injector'
      endtag: '// endinjector'
      addRootSlash: false

    indexFilter = $.filter 'index.less', restore: true

    gulp.src "#{options.src}/app/index.less"
      .pipe $.debug title: 'Inject Styles'
      .pipe indexFilter
      .pipe $.inject(injectFiles, injectOptions)
      .pipe indexFilter.restore()
      .pipe $.sourcemaps.init()
      .pipe($.less lessOptions).on 'error', options.errorHandler 'Less'
      .pipe($.autoprefixer()).on 'error', options.errorHandler 'Autoprefixer'
      .pipe $.sourcemaps.write()
      .pipe gulp.dest("#{options.tmp}/serve/app/")
      .pipe browserSync.reload stream: true
