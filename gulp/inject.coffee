'use strict'

gulp = require('gulp')
$ = require('gulp-load-plugins')()
wiredep = require('wiredep').stream

module.exports = (options) ->
  baseDir = "{#{options.tmp}/serve,#{options.src}}/#{options.app}"

  styles = [
    "#{options.tmp}/serve/#{options.app}/**/*.css"
    "!#{options.tmp}/serve/app/vendor.css"
  ]

  scripts = [
    "#{baseDir}/**/*.module.js"
    "#{baseDir}/**/*.config.js"
    "#{baseDir}/**/i18n/*.js"
    "#{baseDir}/**/*.js"
  ]

  gulp.task 'inject', ['scripts', 'styles', 'markups'], ->
    injectStyles = gulp.src styles, read: false

    injectScripts = gulp.src scripts, read: false

    injectOptions =
      ignorePath: [options.src, "#{options.tmp}/serve"]
      addRootSlash: false

    wiredepOptions =
      directory: 'bower_components'
      exclude: [/jquery/]

    gulp.src "#{options.src}/*.html"
      .pipe $.inject(injectStyles, injectOptions)
      .pipe $.inject(injectScripts, injectOptions)
      .pipe wiredep(wiredepOptions)
      .pipe gulp.dest("#{options.tmp}/serve")

