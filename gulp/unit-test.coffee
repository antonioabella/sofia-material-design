'use strict'

gulp = require('gulp')
$ = require('gulp-load-plugins')()

wiredep = require('wiredep')
KarmaServer = require('karma').Server
concat = require('concat-stream')
_ = require('lodash')

module.exports = (opts) ->
  baseDir = "#{opts.src}/#{opts.app}"

  listFiles = (callback) ->
    bowerDeps = wiredep
      directory: 'bower_components'
      dependencies: true
      devDependencies: true

    specFiles = [
      "#{opts.test}/spec/**/*.coffee"
    ]

    jadeFiles = ["#{baseDir}/**/*.tpl.jade"]

    jsonFiles =
      pattern: "#{opts.test}/fixtures/**/*.json"
      watched: true
      served: true
      included: false

    srcFiles = [
      "#{baseDir}/**/*.module.coffee"
      "#{baseDir}/**/*.config.coffee"
      "#{baseDir}/**/i18n/*.coffee"
      "#{baseDir}/**/*.coffee"
      "#{baseDir}/**/*.js"
    ]

    gulp.src srcFiles
      .pipe concat (files) ->
        callback(
          bowerDeps.js.concat(_.pluck(files, 'path'))
          .concat ["#{opts.test}/fixtures/config.js"]
          .concat jadeFiles
          .concat specFiles
          .concat jsonFiles
        )
      return

  runTests = (singleRun, done) ->
    listFiles (files) ->
      new KarmaServer(
        configFile: "#{__dirname}/../karma.conf.coffee"
        files: files
        singleRun: singleRun
        autoWatch: true
      , done).start()

  gulp.task 'test', (done) ->
    runTests true, done

  gulp.task 'test:auto', (done) ->
    runTests false, done
