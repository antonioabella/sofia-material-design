'use strict'

gulp = require('gulp')
browserSync = require('browser-sync')
$ = require('gulp-load-plugins')()
gutil  = require('gulp-util')

runTask = (task) ->
  (event) ->
    gutil.log event
    if event.type is 'changed' then gulp.start(task) else gulp.start('inject')

module.exports = (opts) ->
  styles = ["#{opts.src}/#{opts.app}/**/*.{css,less}"]
  scripts = ["#{opts.src}/#{opts.app}/**/*.{js,coffee}"]
  markups = ["#{opts.src}/#{opts.app}/**/*.{jade,html}"]

  gulp.task 'watch', ['markups', 'inject'], ->
    gulp.watch ["#{opts.src}/*.html", 'bower.json'], ['inject']

    gulp.watch styles, runTask 'styles'

    gulp.watch scripts, runTask 'scripts'

    $.watch markups, runTask 'markups'

    gulp.watch "#{opts.src}/**/*.html", (event) ->
      browserSync.reload event.path

