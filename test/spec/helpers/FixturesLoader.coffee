'use strict'

class @FixturesLoader
  @months: ->
    getJSONFixture('months.json')

  @costs: ->
    getJSONFixture('costs.json')

  @concepts: ->
    getJSONFixture('concepts.json')

  @enrollments: (index) ->
    getJSONFixture("enrollments/enrollments#{index}.json")

  @conceptsFiltered: (index) ->
    getJSONFixture("conceptsFiltered/filtered#{index}.json")

  @enrollmentsCommitments: ->
    getJSONFixture('enrollments/enrollmentsCommitments.json')

  @resume: (schoolYear) ->
    getJSONFixture("billingResumes/resume#{schoolYear}.json")

  @enrolledCourses: ->
    getJSONFixture('enrolledCourses/enrolledCourses.json')

  @load: (path) ->
    getJSONFixture(path)

  @evaluations: ->
    getJSONFixture('evaluations/evaluations.json')

