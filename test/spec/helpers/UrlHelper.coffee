'use strict'

fullUrl = (source) ->
  return "http://192.168.1.175:8080/#{source}" unless _.contains(source, 'json')
  "app/i18n/#{source}"
