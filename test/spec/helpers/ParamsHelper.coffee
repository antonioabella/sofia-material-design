'use strict'

class ParamsHelper
  @getUrl: (baseUrl, params) ->
    pp = new ParamsProcessor(baseUrl)
    for key, value of params
      pp.process key, value
    pp.getUrl()

class ParamsProcessor
  constructor: (@baseUrl, @extraParams = []) ->
    @_url = @baseUrl

  process: (key, value) ->
    if @_url.indexOf(key) > 0
      @_url = @_url.replace(@_normalizeKey(key), value)
    else
      @extraParams.push "#{key}=#{value}"

  _normalizeKey: (key) ->
    ":#{key}"

  getUrl: ->
    @_cleanUrl() + @_extras()

  _cleanUrl: ->
    url = @_url.replace(/\/$/, '')
    url = url.replace(/\/:\w+/g, '')
    url

  _extras: ->
    return '?' + @extraParams.join('&') if @extraParams.length > 0
    ''
