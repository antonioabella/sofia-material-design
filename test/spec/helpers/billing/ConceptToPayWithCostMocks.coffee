'use strict'

class ConceptToPayWithCostMocks
  @load: (number, ConceptToPay) ->
    switch number
      when 0 then @_generate00(ConceptToPay)

  @_generate00: (ConceptToPay) ->
    [
      # enrollmentId, conceptId, monthId, monthPosition, amount
      new ConceptToPay(6, 1, 11, 3, 90.00)
      new ConceptToPay(6, 1, 12, 4, 90.00)
      new ConceptToPay(6, 1, 1, 5, 90.00)
      new ConceptToPay(6, 1, 2, 6, 90.00)
      new ConceptToPay(6, 1, 3, 7, 90.00)
      new ConceptToPay(6, 1, 4, 8, 90.00)
      new ConceptToPay(6, 1, 5, 9, 100.00)
      new ConceptToPay(6, 1, 6, 10, 100.00)
      new ConceptToPay(6, 1, 7, 11, 100.00)
      new ConceptToPay(6, 1, 8, 12, 100.00)
      new ConceptToPay(6, 2, 6, 10, 500.00)
      new ConceptToPay(6, 2, 7, 11, 500.00)
      new ConceptToPay(6, 2, 8, 12, 500.00)
      new ConceptToPay(6, 3, 4, 8, 200.00)
      new ConceptToPay(7, 1, 13, 0, 70.00)
      new ConceptToPay(7, 1, 9, 1, 90.00)
      new ConceptToPay(7, 1, 10, 2, 90.00)
      new ConceptToPay(7, 1, 11, 3, 90.00)
      new ConceptToPay(7, 1, 12, 4, 90.00)
      new ConceptToPay(7, 1, 8, 12, 100.00)
      new ConceptToPay(7, 2, 3, 7, 300.00)
      new ConceptToPay(7, 2, 4, 8, 300.00)
      new ConceptToPay(7, 2, 8, 12, 500.00)
    ]
