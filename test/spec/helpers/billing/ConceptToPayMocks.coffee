'use strict'

class ConceptToPayMocks
  @load: (number, ConceptToPay) ->
    @ConceptToPay = ConceptToPay
    switch number
      when 0 then @_generate00()
      when 1 then @_generate01()

  @_generate00: =>
    _.flatten [
      @addConcepts 6, 1, [3..11], false
      @addConcepts 6, 2, [10..11]
      @addConcepts 6, 3, [1..11]
      @addConcepts 7, 1, [1..4]
      @addConcepts 7, 2, [7..8], false
    ]

  @_generate01: =>
    _.flatten [
      @addConcepts 1, 1, [4..11], false
      @addConcepts 1, 4, [1..11]
      @addConcepts 1, 5, [1..11]
      @addConcepts 2, 4, [1..3]
      @addConcepts 2, 5, [7..8], false
      @addConcepts 3, 4, [1..4]
      @addConcepts 3, 5, [7..8]
    ]

  @addConcepts: (enrollmentId, conceptId, months, createEnrollment = true) ->
    result = @_createConcepts(enrollmentId, conceptId, months)
    result.push @_createConcept(enrollmentId, conceptId, 12)
    @_addEnrollment(result, enrollmentId, conceptId) if createEnrollment
    result

  @_createConcepts: (enrollmentId, conceptId, months) ->
    @_createConcept(enrollmentId, conceptId, month) for month in months

  @_createConcept: (enrollmentId, conceptId, month) ->
    c = new @ConceptToPay(enrollmentId, conceptId, @_getId(month), month)

  @_getId: (monthPosition) ->
    if monthPosition >= 5 then monthPosition - 4 else monthPosition + 8

  @_addEnrollment: (result, enrollmentId, conceptId) ->
    result.unshift(new @ConceptToPay(enrollmentId, conceptId, 13, 0))
