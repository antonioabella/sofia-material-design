'use strict'

class ConceptToPayHouseholdMocks
  @load: (number) ->
    switch number
      when 0 then @_generate00()

  @_generate00: ->
    _.flatten [
      ConceptToPayMocks.addConcepts 1, 1, [4..11], false
      ConceptToPayMocks.addConcepts 1, 4, [1..11]
      ConceptToPayMocks.addConcepts 1, 5, [1..11], false
    ]
