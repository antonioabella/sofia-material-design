'use strict'

createPromise = (params, result = true) ->
  then: (resolve, reject) ->
    if result then resolve(params) else reject(params)

createRejectPromise = (params) ->
  createPromise params, false
