'use strict'

gulp = require('gulp')
gutil = require('gulp-util')
wrench = require('wrench')

options =
  src: 'src'
  test: 'test'
  dist: 'dist'
  tmp: '.tmp'
  e2e: 'e2e'
  app: '{app,components,directives}'
  errorHandler: (title) ->
    (err) ->
      gutil.log gutil.colors.red("[#{title}]"), err.toString()
      @emit 'end'

wrench.readdirSyncRecursive './gulp'
  .filter (file) ->
    /\.(js|coffee)$/i.test(file)
  .map (file) ->
    require("./gulp/#{file}")(options)

gulp.task 'default', ['clean'], ->
  gulp.start 'build'
