module.exports = (config) ->
  app = '{components,app,directives}'

  config.set
    frameworks: ['jasmine']
    browsers: ['PhantomJS']
    port: 9876

    preprocessors:
      'src/directives/**/*.tpl.jade': 'ng-jade2js'
      'test/spec/**/*.coffee': 'coffee'
      "src/#{app}/**/*.spec.coffee": 'coffee'
      "src/#{app}/**/!(*.spec)+(.coffee)": 'coverage'

    coffeePreprocessor:
      options:
        bare: true
        sourceMap: true
      transformPath: (path) ->
        return path.replace(/.js$/, '.coffee')

    reporters: ['progress', 'html', 'coverage']

    ngJade2JsPreprocessor:
      stripPrefix: 'src/'
      #cacheIdFromPath: (filepath) ->
        #return filepath.replace(/\.jade$/, '.html')
      moduleName: 'sofiaTemplates'

    coverageReporter:
      type: 'html',
      dir: 'test/reports/coverage/'
      instrumenters:
        ibrik: require('ibrik')
      instrumenter:
        '**/*.coffee': 'ibrik'

    htmlReporter:
      outputDir: 'test/reports/html/'
